---
title: "About"
date: 2022-09-25T11:18:00+02:00
draft: false
bookToc: false
---

# DNS in SQL

Since [DNS](https://en.wikipedia.org/wiki/Domain_Name_System) is
foundational on the Internet, and since
[SQL](https://en.wikipedia.org/wiki/SQL) is arguably the most
commonly-used database abstration, DNS data gets stored in
SQL fairly often.

SQL is a well-understood and widely-implemented language for working
with relational databases.  Using SQL to store DNS data allows the use
of robust, high-performance database software that can be accessed by
virtually any programming language.

However, I noticed that the ways in which SQL implementations stored
DNS data were usually quite basic. They either did not support the
full range of DNS, or they did not properly enforce DNS restrictions,
or both.

As an exercise, I decided to see what a full "proper" implementation
of DNS in SQL might look like using modern SQL servers.

---
[Getting Started](articles/01-getting-started/)
