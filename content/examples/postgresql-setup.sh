#! /bin/bash

# Set this to something else, please.
PASSWORD="unsafe"

# Create a user for DNS data.
docker exec -it dns-postgres psql -U postgres --command="CREATE USER dns_sql WITH PASSWORD '${PASSWORD}'"

# Create a database for DNS data.
docker exec -it dns-postgres psql -U postgres --command='CREATE DATABASE dns OWNER=dns_sql'

# Load our schmea.
docker cp postgresql.sql dns-postgres:/tmp/postgresql.sql
docker exec -it dns-postgres psql -U dns_sql dns --file=/tmp/postgresql.sql
