CREATE TABLE dns_type (
    dns_type_id INT PRIMARY KEY CHECK (dns_type_id BETWEEN 0 AND 65535),
    code VARCHAR(10) UNIQUE NOT NULL
);

CREATE INDEX ON dns_type (dns_type_id, code);
CREATE INDEX ON dns_type (code, dns_type_id);

INSERT INTO dns_type (code, dns_type_id) VALUES
  ('A', 1),
  ('NS', 2),
  ('SOA', 6),
  ('AAAA', 28);

CREATE OR REPLACE FUNCTION is_valid_name(labels BYTEA[])
RETURNS BOOLEAN AS $$
DECLARE
  result BOOLEAN DEFAULT TRUE;
BEGIN
  IF labels <> '{}' THEN
    SELECT bool_and((0 < length(label)) AND (length(label) < 64))
       AND ((sum(length(label)) + array_length(labels, 1)) <= 254)
       AND bool_and(lower(encode(label, 'escape')) = encode(label, 'escape'))
    FROM unnest(labels) AS x(label)
    INTO result;
  END IF;
  RETURN result;
END;
$$ LANGUAGE plpgsql RETURNS NULL ON NULL INPUT IMMUTABLE;

CREATE OR REPLACE FUNCTION dns_label_encode (label TEXT)
RETURNS BYTEA AS $$
DECLARE
  result BYTEA DEFAULT '';
  match TEXT[];
  ch TEXT;
  val INT;
BEGIN
  ASSERT label <> '', 'Empty label';
  -- We use regexp_matches() to split the label into either:
  --  '\###', for 3-digit escaped values
  --  '\?', for any other escaped values
  --  '.', for all non-escaped values
  -- We then calculate the ASCII value, and add that to our result BYTEA.
  FOR match IN SELECT regexp_matches(lower(label), '\\\d{3}|\\.|.', 'g') LOOP
    ch := match[1];
    IF length(ch) = 1 THEN
      ASSERT ch <> '\', 'Backslash at end of label';
      val := ascii(ch);
    ELSIF length(ch) = 2 THEN
      val := ascii(substr(ch, 2));
    ELSE
      val := substr(ch, 2)::int;
      ASSERT val <= 255, 'Invalid octet ''' || substr(ch, 2) || '''';
    END IF;
    result := result || set_byte(' '::bytea, 0, val);
  END LOOP;
  RETURN result;
END;
$$ LANGUAGE plpgsql RETURNS NULL ON NULL INPUT IMMUTABLE;

CREATE OR REPLACE FUNCTION dns_name_encode (name TEXT)
RETURNS BYTEA[] AS $$
DECLARE
  result BYTEA[] DEFAULT '{}';
  label TEXT;
BEGIN
  IF name = '.' THEN
    RETURN result;
  ELSIF substr(name, length(name)) = '.' THEN
    name := substr(name, 1, length(name)-1);
  END IF;
  -- https://stackoverflow.com/a/22249126/814127
  FOREACH label IN ARRAY regexp_split_to_array(name, '(?<![^\\](\\\\)*\\)[.]') LOOP
    result := dns_label_encode(label) || result;
  END LOOP;
  RETURN result;
END;
$$ LANGUAGE plpgsql RETURNS NULL ON NULL INPUT IMMUTABLE;

CREATE OR REPLACE FUNCTION dns_label_decode (label BYTEA)
RETURNS TEXT AS $$
DECLARE
  result TEXT DEFAULT '';
  p INT;
  ch INT;
BEGIN
  p := 0;
  WHILE p < length(label) LOOP
    ch := get_byte(label, p);
    -- Unprintable characters (and space) get numerically encoded.
    IF (ch <= 32) OR (ch >= 127) THEN
      result := result || '\' || lpad(ch::TEXT, 3, '0');
    -- Other characters that we want to escape just use backslash.
    ELSIF ch IN (34, 35, 36, 40, 41, 46, 47, 59, 64, 92) THEN
      result := result || '\' || chr(ch);
    -- Non-encoded escaped characters get added "as is".
    ELSE
      result :=  result || chr(ch);
    END IF;
    p := p + 1;
  END LOOP;
  RETURN result;
END;
$$ LANGUAGE plpgsql RETURNS NULL ON NULL INPUT IMMUTABLE;

CREATE OR REPLACE FUNCTION dns_name_decode (name BYTEA[])
RETURNS TEXT AS $$
DECLARE
  result TEXT[] DEFAULT '{}';
  label BYTEA;
BEGIN
  IF name = '{}' THEN
    RETURN '.';
  END IF;
  FOREACH label IN ARRAY name LOOP
    result := dns_label_decode(label) || result;
  END LOOP;
  RETURN array_to_string(result, '.');
END;
$$ LANGUAGE plpgsql RETURNS NULL ON NULL INPUT IMMUTABLE;

CREATE TABLE zone (
    zone_id INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    origin BYTEA[] UNIQUE NOT NULL CHECK (is_valid_name(origin))
);

CREATE OR REPLACE FUNCTION zone_required_rrset ()
RETURNS trigger
AS $$
DECLARE
  rrsets_exist BOOLEAN;
BEGIN
  SELECT EXISTS (
      SELECT rrset_id
      FROM rrset
      WHERE rrset.zone_id=NEW.zone_id AND rrset.dns_type_id=2)
    AND EXISTS (
      SELECT rrset_id
      FROM rrset
      WHERE rrset.zone_id=NEW.zone_id AND rrset.dns_type_id=6)
  INTO rrsets_exist;
  ASSERT rrsets_exist, 'Both SOA and NS required at zone apex';
  RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TABLE rrset (
    rrset_id INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    zone_id INT NOT NULL,
    name BYTEA[] NOT NULL CHECK (is_valid_name(name)),
    dns_type_id INT NOT NULL,
    ttl INT NOT NULL CHECK (ttl BETWEEN 0 AND 2147483647),
    FOREIGN KEY (zone_id) REFERENCES zone(zone_id) ON DELETE CASCADE,
    FOREIGN KEY (dns_type_id) REFERENCES dns_type(dns_type_id),
    CHECK ((dns_type_id <> 6) OR (name = '{}'))
);

CREATE UNIQUE INDEX ON rrset (zone_id, name, dns_type_id);

CREATE CONSTRAINT TRIGGER zone_required_rrset AFTER INSERT
ON zone
INITIALLY DEFERRED
FOR EACH ROW
EXECUTE PROCEDURE zone_required_rrset();

CREATE OR REPLACE FUNCTION rrset_required_delete_check ()
RETURNS trigger
AS $$
DECLARE
  zone_exists BOOLEAN;
BEGIN
  SELECT EXISTS (
      SELECT zone_id
      FROM zone
      WHERE zone.zone_id=OLD.zone_id)
  INTO zone_exists;
  IF zone_exists THEN
    IF OLD.dns_type_id = 6  THEN
      RAISE EXCEPTION 'Cannot delete SOA from zone';
    END IF;
    RAISE EXCEPTION 'Cannot delete NS from zone apex';
  END IF;
  RETURN NEW;
END;
$$ LANGUAGE plpgsql STABLE;

CREATE CONSTRAINT TRIGGER rrset_required_delete_check AFTER DELETE
ON rrset
INITIALLY DEFERRED
FOR EACH ROW
WHEN (((OLD.dns_type_id = 2) OR (OLD.dns_type_id = 6)) AND (OLD.name = '{}'))
EXECUTE FUNCTION rrset_required_delete_check();

CREATE OR REPLACE FUNCTION rrset_required_update_check ()
RETURNS trigger
AS $$
BEGIN
  RAISE EXCEPTION 'Update to RRset removes required record from zone';
  RETURN NEW;
END;
$$ LANGUAGE plpgsql STABLE;

CREATE CONSTRAINT TRIGGER rrset_required_update_check AFTER UPDATE
ON rrset
FOR EACH ROW
WHEN (
    ((OLD.dns_type_id = 2) OR (OLD.dns_type_id = 6)) AND (OLD.name = '{}')
        AND (NEW.name <> '{}')
)
EXECUTE FUNCTION rrset_required_update_check();

CREATE OR REPLACE FUNCTION rrset_type_change_check ()
RETURNS trigger
AS $$
BEGIN
  RAISE EXCEPTION 'Update to RRset changes the type';
  RETURN NEW;
END;
$$ LANGUAGE plpgsql STABLE;

CREATE CONSTRAINT TRIGGER rrset_type_change_check AFTER UPDATE
ON rrset
FOR EACH ROW
WHEN (OLD.dns_type_id <> NEW.dns_type_id)
EXECUTE FUNCTION rrset_type_change_check();
