-- Check case handling when encoding labels.

DO $TEST$
DECLARE
  labels TEXT[] = ARRAY[
    'foo',
    'FOO',
    'fOO',
    'Foo',
    'X',
    'x'
  ];
  label TEXT;
  encoded_label1 BYTEA;
  encoded_label2 BYTEA;
BEGIN
  FOREACH label IN ARRAY labels LOOP
    SELECT dns_label_encode(label) INTO encoded_label1;
    SELECT dns_label_encode(lower(label)) INTO encoded_label2;
    ASSERT encoded_label1 = encoded_label2;
  END LOOP;
END
$TEST$;
