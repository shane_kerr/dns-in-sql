#! /bin/bash

if tty -s; then
  VERBOSE=1
fi

DOCKER_IP_ADDRESS=$(docker inspect dns-postgres | jq '.[0].NetworkSettings.IPAddress')
HOST_IP=${DOCKER_IP_ADDRESS:1:-1}

# If we have a tests specified, use them, otherwise run all test in the directory.
if [ $# -gt 0 ]; then
  TESTS=$*
else
  TESTS=*.run.sql
fi

# Copy our test files into the container.
# TODO: Only copy tests to run?
tar cf - *.sql | docker cp  --archive - dns-postgres:/tmp

# Run each test.
# TODO: Copy a list of tests into the container and run them all with a
#       single "docker exec" statement.
for TEST_FILE in $TESTS; do
  TEST_NAME=$(echo $TEST_FILE | sed 's/.run.*$//')
  if [ $VERBOSE -ne 0 ]; then
    echo $TEST_NAME
  fi

  docker exec --env ON_ERROR_STOP -it dns-postgres \
	  psql --quiet --user=dns_sql --dbname=dns --set="ON_ERROR_STOP=1" --file=/tmp/$TEST_FILE
  RESULT=$?

  CLEANUP_FILE=$(echo $TEST_FILE | sed 's/.run./.clean./')
  if [ -f $CLEANUP_FILE ]; then
    docker exec --env ON_ERROR_STOP -it dns-postgres \
	    psql --quiet --user=dns_sql --dbname=dns --set="ON_ERROR_STOP=1" --file=/tmp/$CLEANUP_FILE
  fi

  if [ $RESULT -ne 0 ]; then
    echo "TEST ${TEST_FILE} FAILED" >&2
    exit 1
  fi
done
