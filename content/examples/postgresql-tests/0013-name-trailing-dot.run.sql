-- Check encoding & decoding names.

DO $TEST$
DECLARE
  names TEXT[] = ARRAY[
    'trailing-dot.',
    'foo.bar.',
    'x.'
  ];
  name TEXT;
  encoded_name BYTEA[];
  decoded_name TEXT;
BEGIN
  FOREACH name IN ARRAY names LOOP
    SELECT dns_name_encode(name) INTO encoded_name;
    SELECT dns_name_decode(encoded_name) INTO decoded_name;
    ASSERT (decoded_name || '.') = name;
  END LOOP;
END
$TEST$;
