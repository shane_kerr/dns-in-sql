-- Check encoding & decoding labels.

DO $TEST$
DECLARE
  labels TEXT[] = ARRAY[
    'foo',
    'x',
    'with\.dot',
    'long-name-12345678901234567890123456789012345678901234567890123',
    'small\000',
    'big\255',
    'hash\#tag'
  ];
  label TEXT;
  encoded_label BYTEA;
  decoded_label TEXT;
BEGIN
  FOREACH label IN ARRAY labels LOOP
    SELECT dns_label_encode(label) INTO encoded_label;
    SELECT dns_label_decode(encoded_label) INTO decoded_label;
    ASSERT decoded_label = label;
  END LOOP;
END
$TEST$;
