-- Check encoding & decoding names.

DO $TEST$
DECLARE
  names TEXT[] = ARRAY[
    -- empty name
    '',
    -- leading dot
    '.test',
    -- trailing extra dot
    'test..',
    -- double dot in middle
    'example..test'
  ];
  name TEXT;
  had_exception BOOLEAN;
BEGIN
  FOREACH name IN ARRAY names LOOP
    had_exception = FALSE;
    BEGIN
      SELECT dns_name_encode(name);
    EXCEPTION
      WHEN assert_failure THEN
        had_exception = TRUE;
    END;

    ASSERT had_exception, 'dns_name_encode() of bogus name did not fail';
  END LOOP;
END
$TEST$;
