-- Check encoding & decoding names.

DO $TEST$
DECLARE
  names TEXT[] = ARRAY[
    'foo',
    'foo.bar',
    'e-mail\.user.soa.test',
    'not\..double-dot',
    'backslash\\.then.dot',
    '.',
    'x',
    'x.y.z',
    '\000.\255.org',
    '1.2.3.4.5.6.7.8.9.0.1.2.3.4.5.6.7.8.9.0.1.2.3.4.5.6.7.8.9.0.1.2.3.4.5.6.7.8.9.0.1.2.3.4.5.6.7.8.9.0.1.2.3.4.5.6.7.8.9.0.1.2.3.4.5.6.7.8.9.0.1.2.3.4.5.6.7.8.9.0.1.2.3.4.5.6.7.8.9.0.1.2.3.4.5.6.7.8.9.0.1.2.3.4.5.6.7.8.9.0.1.2.3.4.5.6.7.8.9.0',
    -- This name is not a valid name, since it has too many characters,
    -- but we don't check that in our conversion routine.
    'long-name-12345678901234567890123456789012345678901234567890123.long-name-12345678901234567890123456789012345678901234567890123.long-name-12345678901234567890123456789012345678901234567890123.long-name-12345678901234567890123456789012345678901234567890123'
  ];
  name TEXT;
  encoded_name BYTEA[];
  decoded_name TEXT;
BEGIN
  FOREACH name IN ARRAY names LOOP
    SELECT dns_name_encode(name) INTO encoded_name;
    SELECT dns_name_decode(encoded_name) INTO decoded_name;
    ASSERT decoded_name = name;
  END LOOP;
END
$TEST$;
