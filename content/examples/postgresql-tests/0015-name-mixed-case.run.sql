-- Check encoding & decoding names with mixed case.

DO $TEST$
DECLARE
  names TEXT[] = ARRAY[
    'MiXed.CASe',
    'X',
    'zzzzzzzzzzzzzzzzzzzzzzzzzzzzzz'
  ];
  name TEXT;
  encoded_name BYTEA[];
  encoded_lower_name BYTEA[];
  encoded_upper_name BYTEA[];
BEGIN
  FOREACH name IN ARRAY names LOOP
    SELECT dns_name_encode(name) INTO encoded_name;
    SELECT dns_name_encode(LOWER(name)) INTO encoded_lower_name;
    SELECT dns_name_encode(UPPER(name)) INTO encoded_upper_name;
    ASSERT encoded_name = encoded_lower_name;
    ASSERT encoded_name = encoded_upper_name;
    ASSERT is_valid_name(encoded_name);
  END LOOP;
END
$TEST$;
