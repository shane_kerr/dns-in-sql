-- Verify name validity checks.

DO $TEST$
DECLARE
  name_tests JSONB[] = ARRAY[
    -- root zone
    '{"name": ".", "valid":true}',
    -- name long but not too long
    '{"name": "long-name-12345678901234567890123456789012345678901234567890123.long-name-12345678901234567890123456789012345678901234567890123.long-name-12345678901234567890123456789012345678901234567890123.long-name-123456789012345678901234567890123456789012345678901", "valid": true}',
    -- name too long
    '{"name": "long-name-12345678901234567890123456789012345678901234567890123.long-name-12345678901234567890123456789012345678901234567890123.long-name-12345678901234567890123456789012345678901234567890123.long-name-1234567890123456789012345678901234567890123456789012", "valid": false}',
    -- label long but not too long
    '{"name":"123456789012345678901234567890123456789012345678901234567890123", "valid": true}',
    -- label too long
    '{"name":"1234567890123456789012345678901234567890123456789012345678901234", "valid": false}',
    -- dns_name_encode() won't allow an empty label, so we will
    -- specify the labels directly that have an empty label
    '{"labels":[""], "valid": false}',
    '{"labels":["w", ""], "valid": false}',
    '{"labels":["x", "", "y"], "valid": false}',
    '{"labels":["", "z"], "valid": false}',
    '{"labels":["UPPER"], "valid": false}'
  ]::JSONB[];
  name_test JSONB;
  labels BYTEA[];
  is_valid BOOLEAN;
BEGIN
  FOREACH name_test IN ARRAY name_tests LOOP
    is_valid := TRUE;
    IF name_test ? 'name' THEN
      -- Decode the text name and check.
      SELECT is_valid_name(dns_name_encode(name_test->>'name')) INTO is_valid;
    ELSIF name_test ? 'labels' THEN
      -- Convert JSONB array into BYTEA[].
      SELECT array_agg(label::bytea)
      FROM jsonb_array_elements_text(name_test->'labels') AS label
      INTO labels;
      -- Check the BYTEA[] labels.
      SELECT is_valid_name(labels) INTO is_valid;
    END IF;
    ASSERT is_valid = (name_test->>'valid')::BOOLEAN;
  END LOOP;
END
$TEST$;
