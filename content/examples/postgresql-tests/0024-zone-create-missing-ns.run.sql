-- Verify we cannot create a zone without an SOA.
DO $TEST$
DECLARE
  had_exception BOOLEAN = FALSE;
BEGIN

  -- We set constraints to immediate which will cause the CHECK() to
  -- execute without a COMMIT. This is necessary since in order to
  -- catch the exeception we use a BEGIN/EXCEPTION block which does
  -- not support COMMIT/ROLLBACK within the block.
  SET CONSTRAINTS ALL IMMEDIATE;

  BEGIN

    WITH new_zone AS (
      INSERT INTO zone (origin)
      VALUES (dns_name_encode('bar.test'))
      RETURNING zone_id
    ),
    build_rows AS (
      SELECT name, dns_type_id, ttl
      FROM (
        VALUES
        (
          '{}'::bytea[],
          6,
          60*5
        )
      ) AS row_data(name, dns_type_id, ttl)
    )
    INSERT INTO rrset (zone_id, name, dns_type_id, ttl)
    SELECT zone_id, name, dns_type_id, ttl
    FROM new_zone, build_rows;

  EXCEPTION
    WHEN assert_failure THEN
      had_exception = TRUE;
  END;

  ASSERT had_exception, 'Check for NS failed';
END
$TEST$;
