-- Create a new zone, with just an SOA and an NS record.
BEGIN;

WITH new_zone AS (
  INSERT INTO zone (origin)
  VALUES (dns_name_encode('foo.test'))
  RETURNING zone_id
),
build_rows AS (
  SELECT name, dns_type_id, ttl
  FROM (
    VALUES
    (
      '{}'::bytea[],
      6,
      60*10
    ),
    (
      '{}'::bytea[],
      2,
      60*5
    )
  ) AS row_data(name, dns_type_id, ttl)
)
INSERT INTO rrset (zone_id, name, dns_type_id, ttl)
SELECT zone_id, name, dns_type_id, ttl
FROM new_zone, build_rows;

COMMIT;

-- Check that everything looks okay.
BEGIN;

DO $TEST$
DECLARE
  test_zone_id INT;
  cnt INT;
BEGIN
  SELECT zone_id
  FROM zone
  WHERE origin=dns_name_encode('foo.test')
  INTO test_zone_id;

  SELECT COUNT(*)
  FROM rrset
  WHERE zone_id=test_zone_id
  INTO cnt;
  ASSERT cnt=2, 'bad overall RRset check';

  SELECT COUNT(*)
  FROM rrset
  WHERE zone_id=test_zone_id AND dns_type_id=6 AND name='{}' AND ttl=600
  INTO cnt;
  ASSERT cnt=1, 'bad SOA check';

  SELECT COUNT(*)
  FROM rrset
  WHERE zone_id=test_zone_id AND dns_type_id=2 AND name='{}' AND ttl=300
  INTO cnt;
  ASSERT cnt=1, 'bad NS check';
END
$TEST$;

ROLLBACK;

-- Cleanup handled in the '.clean.sql' file.
