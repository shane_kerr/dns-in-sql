-- Check encoding of characters quoted with backslash versus encoded as ASCII.

DO $TEST$
DECLARE
  encoded_label_quote BYTEA;
  encoded_label_code BYTEA;
BEGIN
  SELECT dns_label_encode('\9\!\"\.\?@\@\\X\12') INTO encoded_label_quote;
  SELECT dns_label_encode('9\033\034\046\063@\064\092X12') INTO encoded_label_code;
  ASSERT encoded_label_quote = encoded_label_code;
END
$TEST$;
