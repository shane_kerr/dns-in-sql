-- Check encoding errors.

DO $TEST$
DECLARE
  labels TEXT[] = ARRAY[
    -- empty label
    '',
    -- backslash at end of label
    'nope\',
    -- invalid encoded bytes
    '\256',
    '\999'
  ];
  label TEXT;
  had_exception BOOLEAN;
BEGIN
  FOREACH label IN ARRAY labels LOOP
    had_exception = FALSE;
    BEGIN
      SELECT dns_label_encode(label);
    EXCEPTION
      WHEN assert_failure THEN
        had_exception = TRUE;
    END;

    ASSERT had_exception, 'dns_label_encode() of bogus label did not fail';
  END LOOP;
END
$TEST$;
