-- Verify we cannot create a zone without an SOA and an NS.
DO $TEST$
DECLARE
  had_exception BOOLEAN = FALSE;
BEGIN

  -- We set constraints to immediate which will cause the CHECK() to
  -- execute without a COMMIT. This is necessary since in order to
  -- catch the exeception we use a BEGIN/EXCEPTION block which does
  -- not support COMMIT/ROLLBACK within the block.
  SET CONSTRAINTS ALL IMMEDIATE;

  BEGIN
    INSERT INTO zone (origin)
    VALUES (dns_name_encode('bar.test'));
  EXCEPTION
    WHEN assert_failure THEN
      had_exception = TRUE;
  END;

  ASSERT had_exception, 'Check for SOA and NS failed';
END
$TEST$;
