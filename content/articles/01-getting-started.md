---
title: "Getting Started"
date: 2022-09-26
slug: "01-getting-started"
draft: false
---

# What DNS Data Will We Store?

[SQL](https://en.wikipedia.org/wiki/SQL) is a well-understood and
widely-implemented language for working with relational databases.
Storing DNS data in SQL allows the use of robust, high-performance
database software that can be accessed by virtually any programming
language.

For now, we will consider only DNS [authoritative
servers](https://www.rfc-editor.org/rfc/rfc8499.html#page-19),
although storing state used by DNS [recursive
resolvers](https://www.rfc-editor.org/rfc/rfc8499.html#page-17) or
history of DNS queries would also be interesting.

# Which Databases?

This is an effort to put DNS into SQL, so we'll exclude both NoSQL
databases and specialized data structures.

We use Linux for development so we need something that runs there.

We also prefer open source solutions. While both Microsoft SQL and
Oracle have free versions that run on Linux and can even be used in
production, they are proprietary.

Let's have a look at a recent Stack Overflow developer survey and
find out what they use:

https://insights.stackoverflow.com/survey/2021#section-most-popular-technologies-databases

Looking at this list and looking only at the popular open source SQL
databases, we come up with the following four databases:

| Database             | Popularity |
| :------------------- | ---------: |
| MySQL                |      50.2% |
| PostgreSQL           |      40.4% |
| SQLite               |      32.2% |
| MariaDB              |      17.2% |

This list should not be a surprise for anyone familiar with current
database technology.

Throughout this exercise, we'll cover them in this order:

1. PostgreSQL \
   This is the most advanced database on the list, so implementation
   will for the most part be the cleanest and easiest to understand.
2. MySQL \
   This is the most popular database, so we will cover this next.
3. MariaDB \
   This is _nearly_ identical to MySQL, although minor differences in
   the implementation may occasionally have to be addressed.
4. SQLite \
   As an embedded database, SQLite generally has less functionality
   than the others so will result in the most complex implementation.

# Running the Databases

If you are only interested in a specific database and have it
installed, then there should be no additional work for you. If you
want to play around on your own, or do not yet have a database
installed, you can use these instructions.

We have several options for installing the databases:

1. Download the source and build ourselves.
2. Download a package or binary.
3. Install using the system package.
4. Use a Docker image.

Since we are lazy and want to avoid unnecessary work, we will avoid
specific downloads and opt for either using system packages or Docker
images.

While Docker is nice because it puts each database in an isolated
environment, installing using system packages is also straightforward
(at least on Ubuntu).

However, since we cannot install both MySQL and MariaDB on the same
Ubuntu installation via packages, we'll use Docker. This has the
further advantage of being available for any system running Docker.

For the rest of this, we'll assume you have Docker installed. If not,
then see the [Docker installation
instructions](https://docs.docker.com/engine/install/).

# Installing and Setting Up PostgreSQL

The PostgreSQL Docker image is on DockerHub, and described here:

https://hub.docker.com/%5F/postgres/

Start the server:

```sh
docker run --name dns-postgres -e POSTGRES_PASSWORD=unsafe -d postgres:latest
```

It takes a few minutes to initialize.

Now we can connect to it:

```sh
docker exec -it dns-postgres psql -U postgres
```

Go ahead and exit:

```postgres
exit
```

Create a user. We don't use the `createdb` command so that we can set
a password when we add the user, which is necessary to use the `psql`
command outside of the Docker container to connect to it:

```sh
docker exec -it dns-postgres psql -U postgres --command="CREATE USER dns_sql WITH PASSWORD 'unsafe'"
```

Now create a database:

```sh
docker exec -it dns-postgres createdb -U postgres dns
```

And finally let our user work with this database:

```sh
docker exec -it dns-postgres psql -U postgres --command='GRANT ALL PRIVILEGES ON DATABASE dns TO "dns_sql"'
```

Now we can connect to the database as the user:

```sh
docker exec -it dns-postgres psql -U dns_sql dns
```

We now have a user with a database to work with.

# Installing and Setting Up MySQL

The MySQL Docker image is on DockerHub, and described here:

https://hub.docker.com/%5F/mysql/

Start the server:

```sh
docker run --name dns-mysql -e MYSQL_ROOT_PASSWORD=unsafe -d mysql:latest
```

It takes a few minutes to initialize.

Connect as the administrator:

```sh
docker exec -it dns-mysql mysql -uroot -punsafe
```

Create a user:

```mysql
CREATE USER 'dns-sql'@'localhost' IDENTIFIED BY 'unsafe';
GRANT ALL PRIVILEGES ON * . * TO 'dns-sql'@'localhost';
FLUSH PRIVILEGES;
EXIT;
```

Now connect as the user:

```sh
docker exec -it dns-mysql mysql -udns-sql -punsafe
```

And create a database:

```mysql
CREATE DATABASE dns;
EXIT;
```

We now have a user with a database to work with.

# Installing and Setting Up MariaDB

The MariaDB Docker image is on DockerHub, and described here:

https://hub.docker.com/%5F/mariadb/

Start the server:

```sh
docker run -p 127.0.0.1:3306:3306 --name dns-mariadb -e MARIADB_ROOT_PASSWORD=unsafe -d mariadb:latest
```

It takes a few minutes to initialize.

As expected, creating a database on MariaDB is identical to MySQL.

Connect as the administrator:

```sh
docker exec -it dns-mariadb mysql -uroot -punsafe
```

Create a user:

```mysql
CREATE USER 'dns-sql'@'localhost' IDENTIFIED BY 'unsafe';
GRANT ALL PRIVILEGES ON * . * TO 'dns-sql'@'localhost';
FLUSH PRIVILEGES;
EXIT;
```

Now connect as the user:

```sh
docker exec -it dns-mariadb mysql -udns-sql -punsafe
```

And create a database:

```mysql
CREATE DATABASE dns;
EXIT;
```

# Installing and Setting Up SQLite

There is not an "official" SQLite Docker version, which makes sense
since SQLite is intended for embedding into programs. However, the
`sqlite3` command-line tool lets you view and manipulate SQLite
databases. There are tons of unofficial Docker images supporting this.
This one seems to be updated recently and let you work on local files
easily:

https://hub.docker.com/r/sstc/sqlite3

Create the empty database, to give it the correct owner and
permissions:

```sh
touch dns-sqlite.db
```

Now you can start the `sqlite3` tool to manipulate it:

```sh
docker run --name dns-sqlite -it --rm -v $(pwd):/data sstc/sqlite3:latest sqlite3 dns-sqlite.db
```

Unlike the other databases, we do not have to start a server and
connect, so we can re-run this command any time we connect to the
database.

---
[Names](/dns-in-sql/articles/02-names/)
