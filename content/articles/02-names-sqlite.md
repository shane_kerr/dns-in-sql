---
title: "Names - SQLite"
date: 2022-10-09
draft: false
---

# Name Storage - SQLite

SQLite has no built-in array type. There is a JSON type - although this
requires the json1 extension.  JSON objects are stored as ordinary text, which
means that we cannot use it naively:

```sql
sqlite> select json_array('a!', 'b'), json_array('a', 'b');
["a!","b"]|["a","b"]
sqlite> select json_array('a!', 'b') < json_array('a', 'b');
1
```

In this case, we would normally expect `a` to come before `a!`, but
since `!` comes before `"` SQLite treats the array with "a!" as
smaller.

Based on this, it is probably best to store our names in an identical
format to what we use for
[MySQL](/dns-in-sql/articles/02-names-mysql/#adopted-solution-encode-names),
which is to say that we hexadecimal-encoded every character in a
label, then reverse the labels.

The table will look like this:

```sql
CREATE TABLE name (
    name_id INTEGER PRIMARY KEY,
    labels TEXT UNIQUE CHECK (length(labels) <= 503)
);
```

# Invariant Checking - SQLite

SQLite doesn't always support regular expressions (for example it does
not on the Docker image that we are using). It is possible to
implement checks that names are properly formed using triggers and
Common Table Expressions (CTE), however these are relatively complex
so we will assume that we have regular expression support.

We will use similar regular expression that we used on our `CHECK`
constraint for
[MySQL](/dns-in-sql/articles/02-names-mysql/#invariant-checking---mysql):

```sql
CREATE TABLE name (
    name_id INTEGER PRIMARY KEY,
    labels TEXT UNIQUE NOT NULL
        CHECK (
          (length(labels) <= 503)
          AND ((labels = '')
            OR (labels REGEXP '^([0-36-9A-F][0-9A-F]|40|5[B-F]){1,63}(\.([0-36-9A-F][0-9A-F]|40|5[B-F]){1,63})*$')))
);
```

This is not the same expression, since we need to enforce the length
restriction on the name explicitly, and since SQLite does not use
backslash characters for quoting.

Normal insertion and updates remain possible:

```sql
sqlite> INSERT INTO name (labels) VALUES ('666F6F.74657374');
sqlite> UPDATE name
   ...> SET labels='706F6F.74657374' WHERE labels='666F6F.74657374';
```

These functions ensure that only correct names are stored:

```sql
sqlite> -- empty starting label
sqlite> INSERT INTO name (labels) VALUES ('.7476.74657374');
Error: CHECK constraint failed: (length(labels) <= 503)
          AND ((labels = '')
            OR labels REGEXP '^([0-36-9A-F][0-9A-F]|40|5[B-F]){1,63}(\.([0-36-9A-F][0-9A-F]|40|5[B-F]){1,63})*$')
```

```sql
sqlite> -- empty ending label
sqlite> INSERT INTO name (labels) VALUES ('7476.74657374.');
Error: CHECK constraint failed: (length(labels) <= 503)
          AND ((labels = '')
            OR labels REGEXP '^([0-36-9A-F][0-9A-F]|40|5[B-F]){1,63}(\.([0-36-9A-F][0-9A-F]|40|5[B-F]){1,63})*$')
```

```sql
sqlite> -- empty middle label
sqlite> INSERT INTO name (labels) VALUES ('7476..74657374');
Error: CHECK constraint failed: (length(labels) <= 503)
          AND ((labels = '')
            OR labels REGEXP '^([0-36-9A-F][0-9A-F]|40|5[B-F]){1,63}(\.([0-36-9A-F][0-9A-F]|40|5[B-F]){1,63})*$')
```

```sql
sqlite> -- label with wrong number of digits
sqlite> INSERT INTO name (labels) VALUES ('7476.7.74657374');
Error: CHECK constraint failed: (length(labels) <= 503)
          AND ((labels = '')
            OR labels REGEXP '^([0-36-9A-F][0-9A-F]|40|5[B-F]){1,63}(\.([0-36-9A-F][0-9A-F]|40|5[B-F]){1,63})*$')
```

```sql
sqlite> -- uppercase characters
sqlite> UPDATE name
   ...> SET labels='4C6F6F.74657374' WHERE labels='706F6F.74657374';
Error: CHECK constraint failed: (length(labels) <= 503)
          AND ((labels = '')
            OR labels REGEXP '^([0-36-9A-F][0-9A-F]|40|5[B-F]){1,63}(\.([0-36-9A-F][0-9A-F]|40|5[B-F]){1,63})*$')
```

```sql
sqlite> -- non-hexidecimal characters
sqlite> INSERT INTO name (labels) VALUES ('7476.7X.74657374');
Error: CHECK constraint failed: (length(labels) <= 503)
          AND ((labels = '')
            OR labels REGEXP '^([0-36-9A-F][0-9A-F]|40|5[B-F]){1,63}(\.([0-36-9A-F][0-9A-F]|40|5[B-F]){1,63})*$')
sqlite> INSERT INTO name (labels) VALUES ('7476.X4.74657374');
Error: CHECK constraint failed: (length(labels) <= 503)
          AND ((labels = '')
            OR labels REGEXP '^([0-36-9A-F][0-9A-F]|40|5[B-F]){1,63}(\.([0-36-9A-F][0-9A-F]|40|5[B-F]){1,63})*$')
```

```sql
sqlite> -- label too long
sqlite> INSERT INTO name (labels) VALUES (printf('%0128d', '0'));
Error: CHECK constraint failed: (length(labels) <= 503)
          AND ((labels = '')
            OR labels REGEXP '^([0-36-9A-F][0-9A-F]|40|5[B-F]){1,63}(\.([0-36-9A-F][0-9A-F]|40|5[B-F]){1,63})*$')
```

# Encoding & Decoding Presentation-Format - SQLite

SQLite has no stored procedures, so there is no direct way to
implement the encoding and decoding functionality that we have for
PostgreSQL and MySQL. There are approaches that can come close to this
functionality, but we'll leave the implementation of such encoding and
decoding to the applications for SQLite.
