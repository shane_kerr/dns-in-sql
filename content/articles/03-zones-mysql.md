---
title: "Zones - MySQL"
date: 2022-10-05
draft: false
---

# DNS Types - MySQL

DNS types have a name and a number assigned by IANA, which we can
store in a simple table:

```mysql
CREATE TABLE dns_type (
    dns_type_id SMALLINT UNSIGNED PRIMARY KEY,
    code VARCHAR(10) UNIQUE NOT NULL
);
```

And we can populate it with some types that we need to start with:

```mysql
INSERT INTO dns_type (code, dns_type_id) VALUES
  ('NS', 2),
  ('SOA', 6),
  ('AAAA', 28);
```

# Zone Definition - MySQL

A simple zone definition is just the name (using the regular
expression check as described in the [invariant
checking](/dns-in-sql/articles/02-names-mysql/#invariant-checking---mysql)):

```mysql
CREATE TABLE zone (
    zone_id INT AUTO_INCREMENT PRIMARY KEY,
    origin VARCHAR(503) NOT NULL UNIQUE
        CHECK (
          origin REGEXP '^([0-36-9A-F][0-9A-F]|40|5[B-F]){1,63}(\\.([0-36-9A-F][0-9A-F]|40|5[B-F]){1,63})*$'
          OR origin = '')
);
```

For the RRset definition, we include include the zone that the RRset
is in, the type, and the TTL. For the RRset name, we use a relative
name; that is, the name removing the zone name from the end.

For example if we were storing an RRset for `www.foo.test` that was in
the zone `foo.test`, we would only store the name `www`.

We can also use the same name syntax and check for the name of the
RRset:

```mysql
CREATE TABLE rrset (
    rrset_id INT AUTO_INCREMENT PRIMARY KEY,
    zone_id INT NOT NULL,
    name VARCHAR(503) NOT NULL
        CHECK (
          name REGEXP '^([0-36-9A-F][0-9A-F]|40|5[B-F]){1,63}(\\.([0-36-9A-F][0-9A-F]|40|5[B-F]){1,63})*$'
          OR name = ''),
    dns_type_id SMALLINT UNSIGNED NOT NULL,
    ttl INT UNSIGNED NOT NULL CHECK (ttl <= 2147483647),
    FOREIGN KEY (zone_id) REFERENCES zone(zone_id) ON DELETE CASCADE,
    FOREIGN KEY (dns_type_id) REFERENCES dns_type(dns_type_id),
    UNIQUE INDEX (zone_id, name, dns_type_id)
);
```

We have the following checks here:

- Foreign key to enforce that the zone exists.
- Name validity checking for the relative part of the name.
- Foreign key to enforce that the type exists.
- TTL range checking (based on RFC 2181).

The UNIQUE index enforces the protocol-level uniqueness requirement.

We cascade deletes from the zone to RR in that zone. That way deleting
the entry in the zone table will automatically clean up all records
for that zone.

Using these tables we can create a minimal zone.

```mysql
-- Create the zone record
INSERT INTO zone (origin) VALUES (dns_name_encode('foo.example'));
-- Create the SOA RRset
INSERT INTO rrset (zone_id, name, dns_type_id, ttl) VALUES
  (
    (SELECT zone_id FROM zone WHERE origin=dns_name_encode('foo.example')),
    '',
    (SELECT dns_type_id FROM dns_type WHERE code='SOA'),
    60*5
  );
-- Create the NS RRset
INSERT INTO rrset (zone_id, name, dns_type_id, ttl) VALUES
  (
    (SELECT zone_id FROM zone WHERE origin=dns_name_encode('foo.example')),
    '',
    (SELECT dns_type_id FROM dns_type WHERE code='NS'),
    60*5
  );
```

Now our database is starting to look something like a zone:

```mysql
mysql> SELECT dns_name_decode(CONCAT(origin, IF(name = '', '', CONCAT('.', name)))) AS name,
    ->   ttl, dns_type.code AS type
    -> FROM rrset, dns_type, zone
    -> WHERE dns_type.dns_type_id = rrset.dns_type_id
    -> ORDER BY rrset.name, rrset.dns_type_id;
+-------------+-----+------+
| name        | ttl | type |
+-------------+-----+------+
| foo.example | 300 | SOA  |
| foo.example | 300 | NS   |
+-------------+-----+------+
2 rows in set (0.00 sec)
```

# Invariant Checking - MySQL

We have several additional constraints on our data that we should add.

- There must be exactly one `SOA` rrset for the zone.
- There must be an NS rrset with the same name as the zone.

We could create a `CHECK()` which invoked a stored function to
validate the first two constraints. Unfortunately this would need to
be executed at the end of a transaction that created at least 3 rows
(the zone, an rrset for the `SOA`, and an rrset for the NS). This
requires a deferred check, which is not supported in MySQL (although
the ISO SQL:1999 standard _does_ support deferred checks, so in
principle this is possible). Indeed MySQL does not support _any_
deferred constraints.

To work around this limitation, we need to denormalize our data a bit.
Rather than storing our `SOA` and `NS` in a separate table, we will
store them as part of our zone table:

```mysql
CREATE TABLE zone (
    zone_id INT AUTO_INCREMENT PRIMARY KEY,
    origin VARCHAR(503) NOT NULL UNIQUE
        CHECK (
          origin REGEXP '^([0-36-9A-F][0-9A-F]|40|5[B-F]){1,63}(\\.([0-36-9A-F][0-9A-F]|40|5[B-F]){1,63})*$'
          OR origin = ''),
        
    -- SOA fields
    soa_ttl INT UNSIGNED NOT NULL CHECK (soa_ttl <= 2147483647),
    soa_mname VARCHAR(503) NOT NULL
        CHECK (
          soa_mname REGEXP '^([0-36-9A-F][0-9A-F]|40|5[B-F]){1,63}(\\.([0-36-9A-F][0-9A-F]|40|5[B-F]){1,63})*$'
          OR soa_mname = ''),
    soa_rname VARCHAR(503) NOT NULL
        CHECK (
          soa_rname REGEXP '^([0-36-9A-F][0-9A-F]|40|5[B-F]){1,63}(\\.([0-36-9A-F][0-9A-F]|40|5[B-F]){1,63})*$'
          OR soa_rname = ''),
    soa_serial INT UNSIGNED NOT NULL,
    soa_refresh INT UNSIGNED NOT NULL,
    soa_retry INT UNSIGNED NOT NULL,
    soa_expire INT UNSIGNED NOT NULL,
    soa_minimum INT UNSIGNED NOT NULL,

    -- first NS
    ns_ttl INT UNSIGNED NOT NULL CHECK (ns_ttl <= 2147483647),
    ns_nsdname VARCHAR(503) NOT NULL
        CHECK (
          ns_nsdname REGEXP '^([0-36-9A-F][0-9A-F]|40|5[B-F]){1,63}(\\.([0-36-9A-F][0-9A-F]|40|5[B-F]){1,63})*$'
          OR ns_nsdname = '')
);
```

We do not need separate name or type information for the `SOA` or `NS`
values here, as they live at the zone apex and have specific types. We
do need the TTL as that can be different.

Storing the `SOA` and `NS` values with the zone does introduce some
complexity. It means that it is not possible to find all of the
RRset for zone by looking in a single table. Also, for the `NS` values
it means that while the first `NS` value is stored in the zone table,
other `NS` values are stored in a different table. This will slightly
complicate our `NS` data, as we see later.

Now creating a zone is a single `INSERT`:

```mysql
mysql> INSERT INTO zone (
    ->     origin,
    ->     soa_ttl,
    ->     soa_mname, soa_rname, soa_serial,
    ->     soa_refresh, soa_retry, soa_expire, soa_minimum,
    ->     ns_ttl,
    ->     ns_nsdname
    -> ) VALUES (
    ->     dns_name_encode('bar.example'),
    ->     -- SOA
    ->     600,
    ->     dns_name_encode('ns1.bar.example'),
    ->     dns_name_encode('admin.bar.example'),
    ->     UNIX_TIMESTAMP(),
    ->     900,
    ->     300,
    ->     86400*10,
    ->     600,
    ->     -- NS
    ->     600,
    ->     dns_name_encode('ns1.bar.example')
    -> );
Query OK, 1 row affected (0.41 sec)
```

We can also add a check preventing `SOA` types for rows in the `rrset`
table, since these are now stored in the zone record. Likewise, we
will only allow `NS` rrset that are not at the apex:

```mysql
CREATE TABLE rrset (
    rrset_id INT AUTO_INCREMENT PRIMARY KEY,
    zone_id INT NOT NULL,
    name VARCHAR(503) NOT NULL
        CHECK (
          name REGEXP '^([0-36-9A-F][0-9A-F]|40|5[B-F]){1,63}(\\.([0-36-9A-F][0-9A-F]|40|5[B-F]){1,63})*$'
          OR name = ''),
    dns_type_id SMALLINT UNSIGNED NOT NULL,
    ttl INT UNSIGNED NOT NULL CHECK (ttl <= 2147483647),
    FOREIGN KEY (zone_id) REFERENCES zone(zone_id) ON DELETE CASCADE,
    FOREIGN KEY (dns_type_id) REFERENCES dns_type(dns_type_id),
    UNIQUE INDEX (zone_id, name, dns_type_id),
    CHECK ((dns_type_id <> 6) AND ((dns_type_id <> 2) OR (name <> '')))
);
```

We can still create other RRset:

```mysql
mysql> INSERT INTO rrset (zone_id, name, dns_type_id, ttl)
    -> VALUES
    -> (
    ->     (SELECT zone_id FROM zone WHERE origin=dns_name_encode('bar.example')),
    ->     dns_name_encode('subdomain'),
    ->     (SELECT dns_type_id FROM dns_type WHERE code='NS'),
    ->     60*5
    -> ),
    -> (
    ->     (SELECT zone_id FROM zone WHERE origin=dns_name_encode('bar.example')),
    ->     '',
    ->     (SELECT dns_type_id FROM dns_type WHERE code='AAAA'),
    ->     60*5
    -> );
Query OK, 2 rows affected (0.28 sec)
Records: 2  Duplicates: 0  Warnings: 0
```

However we cannot make any other SOA in the zone:

```mysql
mysql> INSERT INTO rrset (zone_id, name, dns_type_id, ttl)
    -> VALUES 
    -> (
    ->     (SELECT zone_id FROM zone WHERE origin=dns_name_encode('bar.example')),
    ->     dns_name_encode('illegal-soa'),
    ->     (SELECT dns_type_id FROM dns_type WHERE code='SOA'),
    ->     60*5
    -> );
ERROR 3819 (HY000): Check constraint 'rrset_chk_3' is violated.
```

And we cannot add an NS at the zone apex:

```mysql
mysql> INSERT INTO rrset (zone_id, name, dns_type_id, ttl)
    -> VALUES 
    -> (
    ->     (SELECT zone_id FROM zone WHERE origin=dns_name_encode('bar.example')),
    ->     '',
    ->     (SELECT dns_type_id FROM dns_type WHERE code='NS'),
    ->     60*5
    -> ); 
ERROR 3819 (HY000): Check constraint 'rrset_chk_3' is violated.
```

Since different RR type have different syntax and semantics, we don't
want an RR to be able to change type after it is created. We can
prevent this with a trigger:

```mysql
DELIMITER $$

CREATE TRIGGER rrset_immutable_type
BEFORE UPDATE
ON rrset FOR EACH ROW
BEGIN
  IF NEW.dns_type_id <> OLD.dns_type_id THEN
    SIGNAL SQLSTATE '45000'
      SET MESSAGE_TEXT = 'RRset cannot change type';
  END IF;
END $$

DELIMITER ;
```

This works:

```mysql
mysql> UPDATE rrset
    -> SET dns_type_id=28 
    -> WHERE
    ->   zone_id=(SELECT zone_id FROM zone WHERE origin=dns_name_encode('bar.example'))
    ->   AND name=dns_name_encode('subdomain');
ERROR 1644 (45000): RRset cannot change type
```

While we unfortunately had to denormalize our data, our MySQL
implementation gives us comprehensive checks on the data, and should
only allow creation of only correct zones.
