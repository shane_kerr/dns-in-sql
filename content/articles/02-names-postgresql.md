---
title: "Names - PostgreSQL"
date: 2022-09-29
draft: false
---

# Name Storage - PostgreSQL

PostgreSQL has a native array format, which means we don't have to do
any fancy encoding of labels in the names to store them:

```postgres
CREATE TABLE name (
    name_id INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    labels BYTEA[] UNIQUE NOT NULL
);
```

We do not add any explicit indexes since by default PostgreSQL will
create an index for a primary key as well as any `UNIQUE` constraints.
It may be worthwhile to add a composite index consisting of both
`name_id` and `labels`, depending on what lookups are used, since such
an index will allow [index-only
scans](https://www.postgresql.org/docs/current/indexes-index-only-scans.html).

We store names in reversed order:

```postgres
INSERT INTO name (labels) VALUES
   ('{"test", "foo"}'),          -- foo.test
   ('{"test", "bar"}'),          -- bar.test
   ('{"test", "tv", "\\x00"}')   -- \000.tv.test
;
```

Since we are using binary storage, the contents are encoded when we do
a bare `SELECT`:

```postgres
dns=> SELECT * FROM name ORDER BY name_id;
 name_id |              labels               
---------+-----------------------------------
       1 | {"\\x74657374","\\x666f6f"}
       2 | {"\\x74657374","\\x626172"}
       3 | {"\\x74657374","\\x7476","\\x30"}
(3 rows)
```

We can present output this in a readable way, although we have to
get each element in our array as a row, encode it, then collect the
rows back into an array, something like this:

```postgres
dns=> SELECT array_agg(plabel) FROM (
dns->   SELECT name_id, encode(unnest(labels), 'escape') AS plabel FROM name) x
dns->   GROUP BY name_id;
     array_agg     
-------------------
 {test,tv,"\\000"}
 {test,bar}
 {test,foo}
(3 rows)
```

# Invariant Checking - PostgreSQL

We can make a function which checks the name:

```postgres
CREATE OR REPLACE FUNCTION is_valid_name(labels BYTEA[])
RETURNS BOOLEAN AS $$
DECLARE
  result BOOLEAN DEFAULT TRUE;
BEGIN
  IF labels <> '{}' THEN
    SELECT bool_and((0 < length(label)) AND (length(label) < 64))
       AND ((sum(length(label)) + array_length(labels, 1)) <= 254)
       AND bool_and(lower(encode(label, 'escape')) = encode(label, 'escape'))
    FROM unnest(labels) AS x(label)
    INTO result;
  END IF;
  RETURN result;
END;
$$ LANGUAGE plpgsql RETURNS NULL ON NULL INPUT IMMUTABLE;
```

This enforces the constraint that each label is between 1 and 63 bytes
long, and that the length of all of the labels added together - along
with a 1-byte length specifier for each - is no more than 254 bytes.
This will leave 1 byte for the terminating `NUL` character in wire
format.

It also checks to ensure that we only have lowercase values. DNS names
are case-insensitive, but the `BYTEA` type has no notion of
case-insensitive comparisons, so we require everything to be stored in
lowercase.

With this function available, this is our new table definition:

```postgres
CREATE TABLE name (
    name_id INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    labels BYTEA[] UNIQUE NOT NULL CHECK (is_valid_name(labels))
);
```

This prevents invalid names:

```postgres
dns=> -- inserting an uppercase name fails
dns=> INSERT INTO name (labels) VALUES ('{"NOPE"}');
ERROR:  new row for relation "name" violates check constraint "name_labels_check"
DETAIL:  Failing row contains (4, {"\\x4e4f5045"}).
```

```postgres
dns=> -- inserting a label longer than 63 bytes fails
dns=> INSERT INTO name (labels) VALUES (ARRAY[lpad('', 64)::bytea]);
ERROR:  new row for relation "name" violates check constraint "name_labels_check"
DETAIL:  Failing row contains (8, {"\\x20202020202020202020202020202020202020202020202020202020202...).
```

```postgres
dns=> -- inserting an empty label fails
dns=> INSERT INTO name (labels) VALUES ('{""}');
ERROR:  new row for relation "name" violates check constraint "name_labels_check"
DETAIL:  Failing row contains (9, {"\\x"}).
```

```postgres
dns=> -- inserting a name that is too long fails
dns=> INSERT INTO name (labels)
dns-> VALUES (
dns(>   dns_name_encode(
dns(>     rpad('', 63, 'a') || '.' ||
dns(>     rpad('', 63, 'b') || '.' ||
dns(>     rpad('', 63, 'c') || '.' ||
dns(>     rpad('', 62, 'd')
dns(>   )
dns(> );
ERROR:  new row for relation "name" violates check constraint "name_labels_check"
DETAIL:  Failing row contains (18, {"\\x64646464646464646464646464646464646464646464646464646464646...).
```

# Encoding a Presentation-Format Label - PostgreSQL

We can make a function to encode a presentation-format label. This is
useful when working directly on the database (rather than via a
program which can easily encode and decode names).

Our argument is the label value, expressed as a `TEXT` type.

Our return value is the encoded label value, expressed as a `BYTEA`
value.

```postgres
CREATE OR REPLACE FUNCTION dns_label_encode (label TEXT)
RETURNS BYTEA AS $$
DECLARE
  result BYTEA DEFAULT '';
  match TEXT[];
  ch TEXT;
  val INT;
BEGIN
  ASSERT label <> '', 'Empty label';
  -- We use regexp_matches() to split the label into either:
  --  '\###', for 3-digit escaped values
  --  '\?', for any other escaped values
  --  '.', for all non-escaped values
  -- We then calculate the ASCII value, and add that to our result BYTEA.
  FOR match IN SELECT regexp_matches(lower(label), '\\\d{3}|\\.|.', 'g') LOOP
    ch := match[1];
    IF length(ch) = 1 THEN
      ASSERT ch <> '\', 'Backslash at end of label';
      val := ascii(ch);
    ELSIF length(ch) = 2 THEN
      val := ascii(substr(ch, 2));
    ELSE
      val := substr(ch, 2)::int;
      ASSERT val <= 255, 'Invalid octet ''' || substr(ch, 2) || '''';
    END IF;
    result := result || set_byte(' '::bytea, 0, val);
  END LOOP;
  RETURN result;
END;
$$ LANGUAGE plpgsql RETURNS NULL ON NULL INPUT IMMUTABLE;
```

We can see how this function works:

```postgres
dns=> -- the BYTEA values look like hex-encoded ASCII
dns=> SELECT dns_label_encode('test');
 dns_label_encode 
------------------
 \x74657374
(1 row)
```

```postgres
dns=> -- uppercase characters are converted to lowercase
dns=> SELECT dns_label_encode('TEst');
 dns_label_encode 
------------------
 \x74657374
(1 row)
```

```postgres
dns=> -- backslash without digits after encodes the next character
dns=> SELECT dns_label_encode('a\.user');
 dns_label_encode 
------------------
 \x612e75736572
(1 row)
```

```postgres
dns=> -- backslash with digits get converted from the ASCII code
dns=> SELECT dns_label_encode('\255\128\000');
 dns_label_encode 
------------------
 \xff8000
(1 row)
```

Invoking with an invalid label will cause an error:

```postgres
dns=> SELECT dns_label_encode('\256');
ERROR:  Invalid octet '256'
CONTEXT:  PL/pgSQL function dns_label_encode(text) line 23 at ASSERT
```

```postgres
dns=> SELECT dns_label_encode('bad-end\');
ERROR:  Backslash at end of label
CONTEXT:  PL/pgSQL function dns_label_encode(text) line 17 at ASSERT
```

Unlike label encoding for other database software, we consider an empty label
an error:

```postgres
dns=> SELECT dns_label_encode('');
ERROR:  Empty label
CONTEXT:  PL/pgSQL function dns_label_encode(text) line 8 at ASSERT
```

Note that we do _not_ check for labels that are too long. That will be
caught by our name validation function.


# Encoding a Presentation-Format Name - PostgreSQL

Now that we can encode a single label, we can use this to encode a
name.

Our argument is the name, expressed as a `TEXT` type. It gets split on
non-escaped `.` (dot) characters. A single trailing dot (used to
indicate an absolute instead of a relative name in DNS) is ignored.
The special name of the root zone, a single `.` (dot) character, is
also handled.

Our return value is the encoded label value, which is an array of
`BYTEA`.

```postgres
CREATE OR REPLACE FUNCTION dns_name_encode (name TEXT)
RETURNS BYTEA[] AS $$
DECLARE
  result BYTEA[] DEFAULT '{}';
  label TEXT;
BEGIN
  IF name = '.' THEN
    RETURN result;
  ELSIF substr(name, length(name)) = '.' THEN
    name := substr(name, 1, length(name)-1);
  END IF;
  -- https://stackoverflow.com/a/22249126/814127
  FOREACH label IN ARRAY regexp_split_to_array(name, '(?<![^\\](\\\\)*\\)[.]') LOOP
    result := dns_label_encode(label) || result;
  END LOOP;
  RETURN result;
END;
$$ LANGUAGE plpgsql RETURNS NULL ON NULL INPUT IMMUTABLE;
```

Using it is straightforward.

```postgres
dns=> -- single label names are identical to the encoded label
dns=> SELECT dns_name_encode('test');
 dns_name_encode 
-----------------
 {"\\x74657374"}
(1 row)
```

```postgres
dns=> -- multiple label names are the labels in reverse order
dns=> SELECT dns_name_encode('www.example.com');
                dns_name_encode                
-----------------------------------------------
 {"\\x636f6d","\\x6578616d706c65","\\x777777"}
(1 row)
```

```postgres
dns=> -- an absolute name is stored the same as a relative name
dns=> SELECT dns_name_encode('absolute.') = dns_name_encode('absolute');
 ?column? 
----------
 t
(1 row)
```

We can use this to manipulate table data from the CLI in a
human-readable way.

```postgres
dns=> insert into name (labels) values
dns->   (dns_name_encode('ipso.test')),
dns->   (dns_name_encode('facto.test')),
dns->   (dns_name_encode('quid.test')),
dns->   (dns_name_encode('pro.test')),
dns->   (dns_name_encode('quo.test'));
INSERT 0 5
```
```postgres
dns=> update name
dns-> set labels=dns_name_encode('amateur.test')
dns-> where labels=dns_name_encode('pro.test');
UPDATE 1
```

Note that we do _not_ check for names that have too many labels or
names that are too long. That will be caught by our name validation
function.

# Decoding a Label to Presentation-Format - PostgreSQL

Now that we can insert or update using a readable format, lets try to
view output in a readable format. As with encoding, we can start off
by labels:

```postgres
CREATE OR REPLACE FUNCTION dns_label_decode (label BYTEA)
RETURNS TEXT AS $$
DECLARE
  result TEXT DEFAULT '';
  p INT;
  ch INT;
BEGIN
  p := 0;
  WHILE p < length(label) LOOP
    ch := get_byte(label, p);
    -- Unprintable characters (and space) get numerically encoded.
    IF (ch <= 32) OR (ch >= 127) THEN
      result := result || '\' || lpad(ch::TEXT, 3, '0');
    -- Other characters that we want to escape just use backslash.
    ELSIF ch IN (34, 35, 36, 40, 41, 46, 47, 59, 64, 92) THEN
      result := result || '\' || chr(ch);
    -- Non-encoded escaped characters get added "as is".
    ELSE
      result :=  result || chr(ch);
    END IF;
    p := p + 1;
  END LOOP;
  RETURN result;
END;
$$ LANGUAGE plpgsql RETURNS NULL ON NULL INPUT IMMUTABLE;
```

As with encoding, usage is straightforward.

```postgres
dns=> -- labels without special characters are identical
dns=> SELECT dns_label_decode('example'::BYTEA);
 dns_label_decode 
------------------
 example
(1 row)
```

```postgres
dns=> -- certain characters get escaped with a backslash
dns=> SELECT dns_label_decode('with.dot'::BYTEA);
 dns_label_decode 
------------------
 with\.dot
(1 row)
```

```postgres
dns=> -- unprintable characters get numerically encoded
dns=> SELECT dns_label_decode('\x0100e8'::BYTEA);
 dns_label_decode 
------------------
 \001\000\232
(1 row)
```

Unlike encoding, there are no possible error conditions.

# Decoding a Name to Presentation-Format - PostgreSQL

Now we can convert an entire name back to presentation format.

This might be slightly more compact with a function that could reverse
an array, but we would have to write that ourselves.

```postgres
CREATE OR REPLACE FUNCTION dns_name_decode (name BYTEA[])
RETURNS TEXT AS $$
DECLARE
  result TEXT[] DEFAULT '{}';
  label BYTEA;
BEGIN
  IF name = '{}' THEN
    RETURN '.';
  END IF;
  FOREACH label IN ARRAY name LOOP
    result := dns_label_decode(label) || result;
  END LOOP;
  RETURN array_to_string(result, '.');
END;
$$ LANGUAGE plpgsql RETURNS NULL ON NULL INPUT IMMUTABLE;
```

Now when we query our table the results are nice and readable:

```postgres
dns=> SELECT dns_name_decode(labels) FROM name ORDER BY labels;
 dns_name_decode 
-----------------
 amateur.test
 facto.test
 ipso.test
 quid.test
 quo.test
(5 rows)
```
