---
title: "Record Data"
date: 2022-10-20
draft: false
---

# Record Data

Now that we are able to store DNS zones and RRset, it is time to store
the actual data stored in DNS resource records, the RDATA.

## Single Table or Multiple Tables?

There are at least two fundamental approaches that can be taken:

1. We can store all of the RDATA in a single table.
2. We can store each type of RDATA in a separate table.

There are several advantages of using a single table:

- Adding a new type does not require adding a table.
- Searching through all data is relatively easy.

There are also several disadvantages of a single table:

- Integrity checking is more complicated, since the checking needs to
  basically be a big case-statement that handles each type.
- Lookups of fixed-sized data (such as AAAA or A records) will be
  slower.

## Record Data Format

In either a single or per-type table, the data can be stored in a
textual format (such as DNS presentation format) or a binary format
(like DNS wire format). Using a textual format has the advantage that
it is easy for humans to read, but tends to be larger than wire format
and also adds computing time if intended to be converted to a binary
format for any reason. The binary format is compact but completely
unreadable by humans; binary format may need to be modified when used
as well, to account for name compression.

If a per-type table is used, then we can use an appropriate SQL data
type for each RR type.

In any case, we can use the standard way of handling unknown DNS RR
types specified in RFC 3597 to store unknown types.

# Approaches

## Possible Solution: Single-Table, Presentation Format

This method is simple. It looks something like this:

```sql
CREATE TABLE rdata (
    rdata_id INT PRIMARY KEY,
    rrset_id INT NOT NULL,
    data VARCHAR(229270) NOT NULL,
    FOREIGN KEY(rrset_id) REFERENCES rrset(rrset_id)
);
```

We just stick in the data as text using the DNS presentation format,
so an `A` record might look something like `'192.0.2.13'`, and an MX
might look like `'10 post.example.org'`. 

Validation of RDATA depends on the type, and involves parsing the
text respresentation. As such it is both complicated and slow.

Note the large size of the `data` column. 

In principle a large `TXT` value could have 65535 bytes. This is
larger than could ever fit in a DNS message, since space is needed for
the header, question section, and the record name, class, type, and
TTL, and size, so we could reduce it a bit. The smallest DNS question
is 5 bytes (1 byte for the name, 2 bytes for the class, and 2 bytes
for the type). The smallest DNS answer is 11 bytes (1 byte for the
name, 2 bytes for the class, 2 bytes for the type, 4 bytes for the
TTL, and 2 bytes for the length of the data).

So the actual largest RDATA is 65509 bytes:

```
 65535  Maximum DNS message size
   -12  DNS header size
 -----  
 65523  
    -5  Minimum question section size
 -----
 65518
   -11  Minimum answer section size
 -----
 65507
```

A `TXT` record has an abitrary amount of strings, each of which has a
length specifier. The biggest `TXT` record could have 32753 strings of
1 byte each.  Each string takes 6 bytes, plus a space to separate each
string, for 229270 bytes maximum.

In principle a new DNS record type could be added which had a
non-compact representation which might take even _more_ space, but
this is unlikely.

## Possible Solution: Single-Table, Wire Format

This is also relatively simple, but even more difficult to validate
within an SQL server, since the validation would involve parsing wire
format data.

It looks something like this:

```sql
CREATE TABLE rdata (
    rdata_id INT PRIMARY KEY,
    rrset_id INT NOT NULL,
    data BLOB(65507) NOT NULL,
    FOREIGN KEY(rrset_id) REFERENCES rrset(rrset_id)
);
```

Here we stick in the wire format of the RDATA, so the `A` record
previously mentioned would look like `'\xc0\x00\x02\x0d'` and the `MX`
would look like `'\x00\x0a\x04post\x07example\x03org\x00'`.

This is fairly useful if we intend to send this data out via the DNS
protocol, however name compression means that we will probably not
want to send out the binary data exactly for all DNS types.

## Adopted Solution: Multiple-Table, Type-Specific Format

In this approach, we make a separate table for each type of RDATA.
Usually these are relatively simple.

An `A` record RDATA table might look like this:

```sql
CREATE TABLE rdata_a (
    rdata_a_id INT PRIMARY KEY,
    rrset_id INT NOT NULL,
    address INT NOT NULL CHECK (address BETWEEN 0 AND 4294967295),
    FOREIGN KEY(rrset_id) REFERENCES rrset(rrset_id)
);
```

An MX record table might look like this:

```sql
CREATE TABLE rdata_mx (
    rdata_mx_id INT PRIMARY KEY,
    rrset_id INT NOT NULL,
    preference INT NOT NULL CHECK (preference BETWEEN 0 an 65535),
    exchange NAME NOT NULL,
    FOREIGN KEY(rrset_id) REFERENCES rrset(rrset_id)
);
```

We use `NAME` here to mean however we decided to store DNS names in
the database, such as described in
[Names](/dns-in-sql/articles/02-names/).

# Implementation Strategy

There are a _lot_ of [DNS
types](https://www.iana.org/assignments/dns-parameters/dns-parameters.xhtml#dns-parameters-4),
something like 70 or so. While ultimately we probably want to
implement them all, having a strategy about which we implement first
will allow our database to be useful earlier.

If you have a specific set of zones that you want to store, then you
can use that to guide which DNS types are implemented in the database
first. However here we propose a more generic approach.

## Minimal Types

We'll start off with the four types that we need for zones:

* `A`
* `NS`
* `SOA`
* `AAAA`

With these types we can perform the most basic requirement of DNS,
which is returning an address for a given DNS name.

## Very Common Types

Next we can add a few types which are very heavily used:

* `CNAME`
* `PTR`
* `MX`
* `TXT`

At this point, we have enough to store a lot of basic records.

## Unknown Record Type Support

DNS supports representing unknown types using a generic approach. This
is specified in [RFC 3597](https://www.rfc-editor.org/rfc/rfc3597) and
basically looks something like this:

```
TYPE731 \# 6 abcdef012345
```

The `TYPE731` indicates an unknown type being used. The `\#` indicates
the use of the generic encoding. The `6` is the number of bytes, and
the bytes themselves are hex-encoded.

DNS originally allowed name-compression of RDATA, but then this was
forbidden for newer types because it created incompatible
implementations. Name-compression was kept for a specified list of DNS
types, and we should next implement these types to support full
unknown record support. These types that we have not yet specified are:

* `MD`
* `MF`
* `MB`
* `MG`
* `MR`
* `MINFO`
* `RP`
* `ASFDB`
* `RT`
* `NSAP-PTR`
* `SIG`
* `PX`
* `NXT`
* `NAPTR`
* `KX`
* `SRV`
* `DNAME`
* `A6`

We remove `HINFO` from the list as it does not have any names to
compress.

At this point we can in principle store and retrieve any type of data,
but using the unknown record approach is basically the same as storing
data in a binary format, which we would like to avoid for ease of use
and ability to check correctness.

## DNSSEC Types

In order to support DNSSEC, we need to handle DNSSEC types:

* `DS`
* `RRSIG`
* `NSEC`
* `DNSKEY`
* `NSEC3`
* `NSEC3PARAM`

The RRSIG record might be more useful added to the resource record
that it signs, rather than as a separate RDATA type.

## Useful Types

At this point we have implemented a lot of obsolete or rarely-used
DNS types so that we can handle name compression when it is needed.
However, we still have a handful of common, useful, or just cool types
to add:

* `SSHFP`
* `DHCID`
* `TLSA`
* `CDS`
* `CDNSKEY`
* `OPENPGPKEY`
* `CSYNC`
* `ZONEMD`
* `SVCB`
* `HTTPS`

## RFC 1035 Completeness

It would be good to complete the types defined in [RFC
1035](https://www.rfc-editor.org/rfc/rfc1035), the oldest non-obsolete
DNS RFC:

* `HINFO`
* `WKS`

## Remaining Current Types

And we can implement the remaining non-obsolete RR that have an RFC:

* `X25`
* `ISDN`
* `NSAP`
* `SIG`
* `KEY`
* `GPOS`
* `LOC`
* `CERT`
* `APL`
* `IPSECKEY`
* `SMIMEA`
* `HIP`
* `SPF`
* `NID`
* `L32`
* `L64`
* `LP`
* `EUI48`
* `EUI64`
* `URI`
* `CAA`
* `AMTRELAY`

## Pseudo-Types

We do _not_ implement pseudo-types, which are not intended for storing
records:

* `OPT`
* `TKEY`
* `TSIG`
* `IXFR`
* `AXFR`
* `MAILB`
* `MAILA`
* `*` (also known as `ANY`)

However, if the intended use of the database is to store DNS messages
as seen in traffic, then it may be useful to add these. `OPT` in
particular will be tricky, as it has a separate registry of types and
specific formats.

## Non-Standardized or Obsolete Types

If a "complete" implementation is desired, then non-standardized or
obsolete types can be implemeted as well.

