---
title: "Names - MySQL"
date: 2022-09-29
draft: false
---

# Name Storage - MySQL

The simplest approach is to store the name as a binary value, with the
labels reversed:

```mysql
CREATE TABLE name (
    name_id INT AUTO_INCREMENT PRIMARY KEY,
    labels VARBINARY(253) NOT NULL
);
```

So, we would store names like this:

```mysql
INSERT INTO name (labels) VALUES
   ('test.foo'),   -- foo.test
   ('test.bar'),   -- bar.test
   ('test.tv.\0')  -- \000.tv.test
;
```

There are two problems with this arrangement:

1. It is possible for a label to have a `.` (dot) in it, since they
   look like separators.
2. Names do not sort properly (for example, the name `abc#.example`
   will come before `abc.example`, while it should come after it).

We need a better solution.

## Possible Solution: MySQL JSON for Arrays

Ideally we would use an array format to store names, as indicated in
the [Storing a Name](/dns-in-sql/articles/02-names/#storing-a-name)
section.

MySQL does not have an array format, but it _does_ have JSON support,
which includes arrays. However there are limitations of the JSON types
make it less than ideal for our purposes:

1. JSON arrays cannot store binary data (rather, MySQL seems to
   convert binary data to encoded values). So we'll have to explicitly
   convert to hexadecimal or something like it anyway.

2. JSON types cannot be indexed directly, and require a generated
   column. (In principle
   [`JSON_VALUE()`](https://dev.mysql.com/doc/refman/8.0/en/json-search-functions.html#function_json-value)
   can be used for this, but it appears to return `NULL` when being
   used to extract `$[*]`.)

3. JSON arrays are less space efficient than a character
   representation.

In the worst case, a JSON array looks like this:

```
'["00", "01", "02", ..., "7F"]'
```

That's for a DNS name with 127 labels, each of 1 byte. Each label
takes 4 characters, so 508 bytes. Also, except for the last label
there is a 2-character separator (', '), for another 252 bytes. Adding
in the brackets, that gives us 508 + 252 + 2 = 762 bytes. This is more
than 50% bigger than the 503 bytes needed for our encoded,
`VARCHAR`-based version below.

## Possible Solution: Labels in a Separate Table, Names Built by Reference

We can store the labels in a table of their own, then build names
using references to them:

```mysql
CREATE TABLE label (
    label_id INT AUTO_INCREMENT PRIMARY KEY,
    value VARBINARY(63) UNIQUE NOT NULL CHECK (LENGTH(value) > 0)
);

CREATE TABLE name (
    name_id INT AUTO_INCREMENT PRIMARY KEY
);

CREATE TABLE name_labels (
    name_id INT NOT NULL,
    label_id INT NOT NULL,
    label_rpos INT NOT NULL CHECK (label_rpos BETWEEN 0 AND 127),
    FOREIGN KEY(name_id) REFERENCES name(name_id),
    FOREIGN KEY(label_id) REFERENCES label(label_id)
);
```

This can indeed be used to store every possible name in the correct
ordering, but it requires many rows in multiple tables, and each
operation on a name requires several SQL commands or complicated
queries involving joins or common table expressions (CTE).

Note that when there are a lot of repeated names and space is at a
premium this may be the best approach. However, both memory and
storage are relatively cheap compared to the sizes of zones these
days, so that is a rare case.

## Adopted Solution: Encode Names

Another approach - and the one that we will use for the rest of this
exercise - is to convert the binary version of the name into a form
that hides the `.` (dot) separator. We could use a variation of the
DNS presentation format for this:


| Presentation         | Fully-Escaped Presentation                           |
| -------------------- | ---------------------------------------------------- |
| <span style="font-family: monospace">foo.test</span>             | <span style="font-family: monospace">\102\111\111.\116\101\115\116</span>                        |
| <span style="font-family: monospace">bar.test</span>             | <span style="font-family: monospace">\098\097\114.\116\101\115\116</span>                        |
| <span style="font-family: monospace">admin\.email.example</span> | <span style="font-family: monospace">\097\100\109\105\110\056\101\109\097\105...</span> |
| <span style="font-family: monospace">\000.tv.test</span>         | <span style="font-family: monospace">\000.\116\118.\116\101\115\116</span>                       |

When used with reversed labels, this will work, but is not especially
readable and each byte of the name may take four bytes in the
fully-escaped version. Note that we need to escape all characters to
ensure proper ordering.

We can chose a more compact encoding. The most compact way to store
these easily would be using the
[Base64](https://en.wikipedia.org/wiki/Base64) format. Unfortunately
the Base64-encoding used by MySQL's
[`TO_BASE64()`](https://dev.mysql.com/doc/refman/8.0/en/string-functions.html#function_to-base64)
and 
[`FROM_BASE64()`](https://dev.mysql.com/doc/refman/8.0/en/string-functions.html#function_from-base64)
yields encoded values that do not sort in the same order as the
non-encoded values.

Instead, let's convert each byte to the hexadecimal value. This would
look something like this:

| Presentation         | Hex-Encoded                           |
| -------------------- | ------------------------------------- |
| <span style="font-family: monospace">foo.test</span>             | <span style="font-family: monospace">666F6F.74657374</span>                       |
| <span style="font-family: monospace">bar.test</span>             | <span style="font-family: monospace">626172.74657374</span>                       |
| <span style="font-family: monospace">admin\.email.example</span> | <span style="font-family: monospace">61646D696E2E656D61696C.6578616D706C65</span> |
| <span style="font-family: monospace">\000.tv.test</span>         | <span style="font-family: monospace">00.7476.74657374</span>                      |

This is still not easily readable, but it does match the rest of our
requirements when reversed (and people experienced with reading
packet dumps or debugging memory directly will find it legible... in a
way). Sort order is preserved, and `.` (dot) comes before any encoded
character.

Note that both the fully-encoded presentation format and the
hex-encoded format are case-sensitive, so we have to be careful when
using these for storage or comparison. That is, the data must always
be set to lower case before searched for or stored in the database.

We need more space since we use 2 bytes per unencoded byte, meaning
that we can have up to 503 bytes in an encoded name:

```mysql
CREATE TABLE name (
    name_id INT AUTO_INCREMENT PRIMARY KEY,
    labels VARCHAR(503) NOT NULL,
    UNIQUE INDEX (labels)
);
```

We index our `labels` column, otherwise searching our names will
require scanning the entire database.

Using the `UNIQUE` value here means that a given name can only be
inserted  once. For our purposes this is a reasonable space-saving
measure, but this restriction can be removed if there is a need to
have duplicate names (for example if a name can be changed via
`UPDATE`, but changing it for all rows that refer to it would be bad).

These encoded names are a bit cumbersome to work with, especially as
an administrator working directly via SQL, so it would be handy if we
had a way to convert to and from our encoded names. We can make
functions to do this (see below).

# Invariant Checking - MySQL

We should ensure that our names are formatted correctly. The field has
a maximum size that will prevent us from having names that are too
big, but we still have to make sure that each label is at most 63
bytes when decoded, and also that each label is properly hexadecimal
encoded.

Since we are using the hex-encoded `VARCHAR` method for storing names,
we can use a regular expression check for this. We'll check that we
only have pairs of hexadecimal characters (each representing a byte),
and that we have between 1 and 63 of these for each label. Any number
of labels will be allowed, since our length constraint is enforced by
the column type definition. It looks something like this:

```
^([0-9A-F]{2}){1,63}(\.([0-9A-F]{2}){1,63})*$
```

However, we do not want to allow encoded uppercase letters, since we
want those converted to lowercase to handle case-insensitivity. These
are hexadecimal values 41 to 5A. So each of our hexadecimal values
must match:

```
[0-36-9A-F][0-9A-F]|40|5[B-F]
```

That is, anything starting with 0-4, 6-F, the value 40, and values
5B-5F.

Our regular expression is not exactly pretty, but it does allow all
valid names and reject all invalid names:

```
^([0-36-9A-F][0-9A-F]|40|5[B-F]){1,63}(\.([0-36-9A-F][0-9A-F]|40|5[B-F]){1,63})*$
```


We also want to allow for an empty labels value to represent the root
zone.

Lets make another version of our name table, including this check:

```mysql
CREATE TABLE name (
    name_id INT AUTO_INCREMENT PRIMARY KEY,
    labels VARCHAR(503) NOT NULL
        CHECK (
          labels REGEXP '^([0-36-9A-F][0-9A-F]|40|5[B-F]){1,63}(\\.([0-36-9A-F][0-9A-F]|40|5[B-F]){1,63})*$'
          OR labels = ''),
    UNIQUE INDEX (labels)
);
```

Adding our table as before works as expected:

```mysql
mysql> INSERT INTO name (labels) VALUES
    ->   ('666F6F.74657374'),
    ->   ('61646D696E2E656D61696C.6578616D706C65'),
    ->   ('00.7476.74657374');
Query OK, 3 rows affected (0.27 sec)
Records: 3  Duplicates: 0  Warnings: 0
```

Trying to insert invalid values now fails:

```mysql
mysql> -- non-hexadecimal values
mysql> INSERT INTO name (labels) VALUES ('FG');
ERROR 3819 (HY000): Check constraint 'name_chk_1' is violated.
```
```mysql
mysql> -- uneven number of hexadecimal values
mysql> INSERT INTO name (labels) VALUES ('AA.B');
ERROR 3819 (HY000): Check constraint 'name_chk_1' is violated.
```
```mysql
mysql> -- empty label at end
mysql> INSERT INTO name (labels) VALUES ('AA.BB.');
ERROR 3819 (HY000): Check constraint 'name_chk_1' is violated.
```
```mysql
mysql> -- empty label at start
mysql> INSERT INTO name (labels) VALUES ('.AA.BB');
ERROR 3819 (HY000): Check constraint 'name_chk_1' is violated.
```
```mysql
mysql> -- empty label in the middle
mysql> INSERT INTO name (labels) VALUES ('AA..BB');
ERROR 3819 (HY000): Check constraint 'name_chk_1' is violated.
```
```mysql
mysql> -- uppercase label, 0x42 corresponds to 'B'
mysql> INSERT INTO name (labels) VALUES ('42');
ERROR 3819 (HY000): Check constraint 'name_chk_1' is violated.
```

# Encoding a Presentation-Format Label - MySQL

We can make a function to encode a presentation-format label. This is
useful when working directly on the database (rather than via a
program).

Our argument is the label value. A label can be at most 63 bytes long,
but in presentation format each escaped byte is four times as big, so
that's 252 bytes.

Our return value is the encoded label value. Since we convert the label
value to hexadecimal, that is twice as long as the maximum 63-byte
label, or 126 bytes.

```mysql
DELIMITER $$

CREATE FUNCTION dns_label_encode (label VARCHAR(252))
RETURNS VARCHAR(126) DETERMINISTIC NO SQL
BEGIN
  DECLARE encoded_label VARCHAR(126);
  DECLARE p INT;
  DECLARE backslash INT;
  DECLARE code VARCHAR(3);

  SET encoded_label = '';
  SET p = 1;
  scan: LOOP

    -- Find the next backslash in our label.
    SET backslash = LOCATE('\\', label, p);

    -- If no backslash, then we can hex-encode the rest and quit.
    IF backslash = 0 THEN
      SET encoded_label = CONCAT(encoded_label,
                                 HEX(LCASE(SUBSTR(label, p))));
      LEAVE scan;
    END IF;

    -- We found a backslash, see if we have a 3-digit sequence.
    SET code = SUBSTR(label, backslash+1, 3);
    IF code REGEXP '^[0-9]{3}$' THEN
      IF code > 255 THEN
        SET @ERRMSG = CONCAT('Invalid octet ''', code, '''');
        SIGNAL SQLSTATE '45000'
          SET MESSAGE_TEXT = @ERRMSG;
      END IF;
      SET encoded_label = CONCAT(encoded_label,
                                 HEX(LCASE(SUBSTR(label, p, backslash-p))),
                                 HEX(LCASE(CHAR(code))));
      SET p = backslash + 4;

    -- If we don't have a 3-digit sequence, it's another escaped character.
    ELSE
      -- If the backslash is at the end of the string, this is an error.
      IF backslash = LENGTH(label) THEN
        SIGNAL SQLSTATE '45000'
          SET MESSAGE_TEXT = 'Backslash at end of label';
      END IF;

      SET encoded_label = CONCAT(encoded_label,
                                 HEX(LCASE(SUBSTR(label, p, backslash-p))),
                                 HEX(LCASE(SUBSTR(label, backslash+1, 1))));
      SET p = backslash + 2;

    END IF;

  END LOOP;
  RETURN encoded_label;
END $$

DELIMITER ;
```

Before we look at using the function, a warning.

> &#x26a0; **Warning**
> 
> MySQL interprets backslashes in strings, unless the
> [`NO_BACKSLASH_ESCAPES`](https://dev.mysql.com/doc/refman/8.0/en/sql-mode.html#sqlmode_no_backslash_escapes)
> mode is set.
> 
> Examples on this page assume the default, so there are a lot of `\`
> to escape the backslashes themselves.

We can see how this function works:

```mysql
mysql> -- the VARCHAR is indeed hex-encoded ASCII
mysql> SELECT dns_label_encode('test');
+--------------------------+
| dns_label_encode('test') |
+--------------------------+
| 74657374                 |
+--------------------------+
1 row in set (0.00 sec)
```

```mysql
mysql> -- uppercase characters are converted to lowercase
mysql> SELECT dns_label_encode('TEst');
+--------------------------+
| dns_label_encode('TEst') |
+--------------------------+
| 74657374                 |
+--------------------------+
1 row in set (0.00 sec)
```

```mysql
mysql> -- backslash without digits after encodes the next character
mysql> SELECT dns_label_encode('a\\.user');
+------------------------------+
| dns_label_encode('a\\.user') |
+------------------------------+
| 612E75736572                 |
+------------------------------+
1 row in set (0.33 sec)
```

```mysql
mysql> -- backslash with digits get converted from the ASCII code
mysql> SELECT dns_label_encode('\\255\\128\\000');
+-------------------------------------+
| dns_label_encode('\\255\\128\\000') |
+-------------------------------------+
| FF8000                              |
+-------------------------------------+
1 row in set (0.43 sec)
```

We do see that the decoding function is slow when there are encoded
characters. Performance is not currently a primary consideration, but
this may need to be revisited.

Invoking with an invalid label will cause an error:

```mysql
mysql> SELECT dns_label_encode('\\256');
ERROR 1644 (45000): Invalid octet '256'
```
```mysql
mysql> SELECT dns_label_encode('bad-end\\');
ERROR 1644 (45000): Backslash at end of label
```

# Encoding a Presentation-Format Name - MySQL

Now that we can encode a single label, we can use this to encode a
name.

Our argument is the name, which can be up to 1003 characters.

Our return value is the encoded label value, which can be up to 503
characters.

```mysql
DELIMITER $$
CREATE FUNCTION dns_name_encode (name VARCHAR(1003))
RETURNS VARCHAR(503) DETERMINISTIC NO SQL
BEGIN
  DECLARE encoded_name VARCHAR(503);
  DECLARE p INT;
  DECLARE dot INT;

  -- Handle the root zone.
  IF name = '.' THEN
    RETURN '';
  END IF;

  SET encoded_name = '';
  SET p = 1;
  scan: LOOP

    -- If there is a '.' at the beginning of a label, then we have
    -- an empty label. This is an error.
    IF SUBSTR(name, p, 1) = '.' THEN
      SIGNAL SQLSTATE '45000'
        SET MESSAGE_TEXT = 'Empty label in name';
    END IF;

    -- Find the next non-escaped '.' in our name.
    SET dot = REGEXP_INSTR(name, '[^\\\\]\\.', p);

    -- If no more '.', prepend the final encoded label and we're done
    IF dot = 0 THEN
      SET encoded_name = CONCAT(dns_label_encode(SUBSTR(name, p)),
                                encoded_name);
      LEAVE scan;
    END IF;

    -- Otherwise, prepend the encoded label with a '.' before it.
    SET encoded_name = CONCAT('.', 
                              dns_label_encode(SUBSTR(name, p, dot-p+1)),
                              encoded_name);
    SET p = dot + 2;

    -- Handle a trailing '.'.
    IF p > LENGTH(name) THEN
      SET encoded_name = SUBSTR(encoded_name, 2);
      LEAVE scan;
    END IF;

  END LOOP;
  RETURN encoded_name;
END $$

DELIMITER ;
```

The function will convert DNS names to our encoded format.

```mysql
mysql> -- single label names are identical to the encoded label
mysql> SELECT dns_name_encode('test');
+-------------------------+
| dns_name_encode('test') |
+-------------------------+
| 74657374                |
+-------------------------+
1 row in set (0.00 sec)
```

```mysql
mysql> -- multiple label names encode the labels in reverse order
mysql> SELECT dns_name_encode('www.example.com');
+------------------------------------+
| dns_name_encode('www.example.com') |
+------------------------------------+
| 636F6D.6578616D706C65.777777       |
+------------------------------------+
1 row in set (0.00 sec)
```

```mysql
mysql> -- an absolute name is stored the same as a relative name
mysql> SELECT dns_name_encode('absolute.') = dns_name_encode('absolute');
+------------------------------------------------------------+
| dns_name_encode('absolute.') = dns_name_encode('absolute') |
+------------------------------------------------------------+
|                                                          1 |
+------------------------------------------------------------+
1 row in set (0.00 sec)
```

```mysql
mysql> -- empty labels cause errors
mysql> SELECT dns_name_encode('bad..test');
ERROR 1644 (45000): Empty label in name
```

Now we can add data to our name table in a way that makes sense to
humans:

```mysql
INSERT INTO name (labels) VALUES
  (dns_name_encode('foo.example')),
  (dns_name_encode('my\\.email.bar.test')),
  (dns_name_encode('baz.'));
```

This populates our table as expected:

```mysql
mysql> SELECT * FROM name ORDER BY name_id;
+---------+----------------------------------+
| name_id | labels                           |
+---------+----------------------------------+
|       1 | 6578616D706C65.666F6F            |
|       2 | 74657374.626172.6D792E656D61696C |
|       3 | 62617A                           |
+---------+----------------------------------+
3 rows in set (0.00 sec)
```

# Decoding a Label to Presentation-Format - MySQL

Now that we can insert or update using a readable format, lets try to
display encoded names in a readable format. As with encoding, we can
start off by labels:

```mysql
DELIMITER $$

CREATE FUNCTION dns_label_decode (label VARCHAR(126))
RETURNS VARCHAR(252) DETERMINISTIC NO SQL
BEGIN
  DECLARE decoded_label VARCHAR(252);
  DECLARE p INT;
  DECLARE ch BINARY;
  DECLARE ch_val INT;

  -- We could possibly use a regular expression search for any
  -- character that we need to escape, rather than looping over every
  -- character in the label. We would have to benchmark to see which
  -- is the most efficient.
  SET decoded_label = '';
  SET p = 1;
  WHILE p < LENGTH(label) DO

    SET ch = UNHEX(SUBSTR(label, p, 2));
    SET ch_val = ORD(ch);
    -- Unprintable characters (and space) get numerically encoded.
    IF (ch_val <= 32) OR (ch_val >= 127) THEN
      SET decoded_label = CONCAT(decoded_label, '\\', LPAD(ch_val, 3, '0'));
    ELSE
      -- Other characters that we want to escape just use backslash.
      IF ch_val IN (34, 35, 36, 40, 41, 46, 47, 59, 64, 92) THEN
        SET decoded_label = CONCAT(decoded_label, '\\', ch);
      -- Non-encoded escaped characters get added "as is".
      ELSE
        SET decoded_label = CONCAT(decoded_label, ch);
      END IF;
    END IF;

    SET p = p + 2;
  END WHILE;
  RETURN decoded_label;
END $$

DELIMITER ;
```

This function inverts the encoding process.

No specific error checking is done, but when invoked with something
other than an encoded label it may silently ignore invalid data or
fail with an error.

# Decoding a Name to Presentation-Format - MySQL

Now we can convert an entire name back to presentation format.

```mysql
DELIMITER $$
CREATE FUNCTION dns_name_decode (name VARCHAR(503))
RETURNS VARCHAR(1003) DETERMINISTIC NO SQL
BEGIN
  DECLARE decoded_name VARCHAR(1003);
  DECLARE p INT;
  DECLARE dot INT;

  IF name = '' THEN
    RETURN '.';
  END IF;
  
  SET decoded_name = '';
  SET p = 1;
  scan: LOOP

    SET dot = LOCATE('.', name, p);

    IF dot = 0 THEN
      SET decoded_name = CONCAT(dns_label_decode(SUBSTR(name, p)),
                                '.', decoded_name);
      LEAVE scan;
    END IF;

    SET decoded_name = CONCAT(dns_label_decode(SUBSTR(name, p, dot-p)),
                              '.', decoded_name);
    SET p = dot + 1;
  END LOOP;

  RETURN SUBSTR(decoded_name, 1, LENGTH(decoded_name)-1);
END $$

DELIMITER ;
```

We see that this gives us the more friendly form:

```mysql
mysql> SELECT dns_name_decode(labels) FROM name ORDER BY name_id;
+-------------------------+
| dns_name_decode(labels) |
+-------------------------+
| foo.example             |
| my\.email.bar.test      |
| baz                     |
+-------------------------+
3 rows in set (0.00 sec)
```

