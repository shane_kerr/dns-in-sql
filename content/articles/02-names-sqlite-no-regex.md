---
title: "Names - SQLite"
date: 2022-09-29
draft: false
---

# Name Storage - SQLite

SQLite has no built-in array type. There is a JSON type - although this
requires the json1 extension.  JSON objects are stored as ordinary text, which
means that we cannot use it naively:

```sql
sqlite> select json_array('a!', 'b'), json_array('a', 'b');
["a!","b"]|["a","b"]
sqlite> select json_array('a!', 'b') < json_array('a', 'b');
1
```

In this case, we would normally expect `a` to come before `a!`, but
since `!` comes before `"` SQLite treats the array with "a!" as
smaller.

Based on this, it is probably best to store our names in an identical
format to what we use for
[MySQL](http://localhost:1313/dns-in-sql/articles/02-names-mysql/#adopted-solution-encode-names),
which is to say that we hexadecimal-encoded every character in a
label, then reverse the labels.

The table will look like this:

```sql
CREATE TABLE name (
    name_id INTEGER PRIMARY KEY,
    labels TEXT UNIQUE CHECK (LENGTH(labels) <= 503)
);
```

# Invariant Checking - SQLite

SQLite doesn't always support regular expressions (for example it does
not on the Docker image that we are using). If it did, we could use
the same or similar regular expression that we used on our `CHECK`
constraint for
[MySQL](http://localhost:1313/dns-in-sql/articles/02-names-mysql/#invariant-checking---mysql).
We can restrict the characters in the value via the `GLOB` operator,
which uses a Unix file globbing syntax.

SQLite _does_ support triggers, which can execute SQL to validate a
new row. We can use SQLite's support for both ordinary and recursive
Common Table Expressions (CTE) to split our name up into labels, check
that each label has a valid length, and then make sure that the label
consists of hex-encoded characters that exclude upper-case characters.

Here is trigger which implements these checks. It can almost certainly
be made more elegant and performant, but it should correctly enforce
that only properly encoded DNS names are allowed:

```sql
CREATE TRIGGER validate_name_insert
BEFORE INSERT
ON name
WHEN NEW.labels <> ''
BEGIN
  -- The first CTE gets a list of (start, end) offsets of each label.
  -- So for 'AA.BBCC.DDEEFF' we would get:
  --  1, 3
  --  4, 8
  --  9, 15
  WITH RECURSIVE label_pos (i, j) AS (
      SELECT
        1, iif(instr(NEW.labels, '.') = 0, 
               length(NEW.labels)+1,
               instr(NEW.labels, '.'))
    UNION ALL
      SELECT
        j+1, iif(instr(substr(NEW.labels, j+1), '.') = 0,
                 length(NEW.labels)+1,
                 j+instr(substr(NEW.labels, j+1), '.'))
      FROM label_pos WHERE (j <= length(NEW.labels))
  ),
  -- The second CTE extracts out the labels from the name, based on
  -- the offsets from the first CTE. It has a bit of special-casing
  -- to get empty labels.
  name_labels (label) AS (
    SELECT 
      CASE 
        WHEN j>i THEN
          substr(NEW.labels, i, j-i)
        ELSE
          ''
      END AS label
    FROM label_pos
  ), 
  -- The third CTE is where we actually check label lengths. We
  -- look for too short, too long, and we also make sure that we 
  -- have an even number of characters (since everything is supposed
  -- to be encoded as 2 hex digits.
  label_length_check AS (
    SELECT
      CASE
        WHEN length(label) = 0 THEN
          RAISE (ABORT, 'Empty label in name')
        WHEN length(label) > 126 THEN
          RAISE (ABORT, 'Label too long')
        WHEN (length(label) % 2) <> 0 THEN
          RAISE (ABORT, 'Improperly hex-encoded name')
      END AS len_check
    FROM name_labels
  ),
  -- The fourth CTE is where separate the label into hex characters.
  hex_loop(i, label, hex_ch) AS ( 
      SELECT 1, label, substr(label, 1, 2)
      FROM name_labels
      WHERE length(label) > 0
    UNION ALL
      SELECT i+2, hex_loop.label, substr(hex_loop.label, i+2, 2)
      FROM name_labels, hex_loop
      WHERE (i < length(hex_loop.label)-1)
            AND (hex_loop.label = name_labels.label)
  ),
  -- The fifth CTE is where we verify that all of the character are 
  -- valid hex characters and that they do not represent an uppercase
  -- character.
  lowercase_check AS (
    SELECT 
      CASE
        WHEN substr(hex_ch, 1, 1) NOT IN 
            ('0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F')
            THEN
          RAISE (ABORT, 'Non-hexidecimal character in encoded name')
        WHEN substr(hex_ch, 2, 1) NOT IN 
            ('0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F')
            THEN
          RAISE (ABORT, 'Non-hexidecimal character in encoded name')
        WHEN hex_ch BETWEEN '41' AND '5A' THEN
          RAISE (ABORT, 'Name not lowercase')
      END AS case_check
    FROM hex_loop
  )
  -- Finally, we have to use both of our check CTE otherwise they 
  -- will not get invoked.
  SELECT * FROM lowercase_check, label_length_check;
END;
```

This trigger only acts on `INSERT`. To ensure that the data is not
modified via `UPDATE` into an invalid state, we need an almost
identical trigger for that:

```sql
CREATE TRIGGER validate_name_update
BEFORE UPDATE
ON name
WHEN NEW.labels <> ''
BEGIN
  -- The first CTE gets a list of (start, end) offsets of each label.
  -- So for 'AA.BBCC.DDEEFF' we would get:
  --  1, 3
  --  4, 8
  --  9, 15
  WITH RECURSIVE label_pos (i, j) AS (
      SELECT
        1, iif(instr(NEW.labels, '.') = 0, 
               length(NEW.labels)+1,
               instr(NEW.labels, '.'))
    UNION ALL
      SELECT
        j+1, iif(instr(substr(NEW.labels, j+1), '.') = 0,
                 length(NEW.labels)+1,
                 j+instr(substr(NEW.labels, j+1), '.'))
      FROM label_pos WHERE (j <= length(NEW.labels))
  ),
  -- The second CTE extracts out the labels from the name, based on
  -- the offsets from the first CTE. It has a bit of special-casing
  -- to get empty labels.
  name_labels (label) AS (
    SELECT 
      CASE 
        WHEN j>i THEN
          substr(NEW.labels, i, j-i)
        ELSE
          ''
      END AS label
    FROM label_pos
  ), 
  -- The third CTE is where we actually check label lengths. We
  -- look for too short, too long, and we also make sure that we 
  -- have an even number of characters (since everything is supposed
  -- to be encoded as 2 hex digits.
  label_length_check AS (
    SELECT
      CASE
        WHEN length(label) = 0 THEN
          RAISE (ABORT, 'Empty label in name')
        WHEN length(label) > 126 THEN
          RAISE (ABORT, 'Label too long')
        WHEN (length(label) % 2) <> 0 THEN
          RAISE (ABORT, 'Improperly hex-encoded name')
      END AS len_check
    FROM name_labels
  ),
  -- The fourth CTE is where separate the label into hex characters.
  hex_loop(i, label, hex_ch) AS ( 
      SELECT 1, label, substr(label, 1, 2)
      FROM name_labels
      WHERE length(label) > 0
    UNION ALL
      SELECT i+2, hex_loop.label, substr(hex_loop.label, i+2, 2)
      FROM name_labels, hex_loop
      WHERE (i < length(hex_loop.label)-1)
            AND (hex_loop.label = name_labels.label)
  ),
  -- The fifth CTE is where we verify that all of the character are 
  -- valid hex characters and that they do not represent an uppercase
  -- character.
  lowercase_check AS (
    SELECT 
      CASE
        WHEN substr(hex_ch, 1, 1) NOT IN 
            ('0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F')
            THEN
          RAISE (ABORT, 'Non-hexidecimal character in encoded name')
        WHEN substr(hex_ch, 2, 1) NOT IN 
            ('0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F')
            THEN
          RAISE (ABORT, 'Non-hexidecimal character in encoded name')
        WHEN hex_ch BETWEEN '41' AND '5A' THEN
          RAISE (ABORT, 'Name not lowercase')
      END AS case_check
    FROM hex_loop
  )
  -- Finally, we have to use both of our check CTE otherwise they 
  -- will not get invoked.
  SELECT * FROM lowercase_check, label_length_check;
END;
```

Normal insertion and updates remain possible:

```sql
sqlite> INSERT INTO name (labels) VALUES ('666F6F.74657374');
sqlite> UPDATE name
   ...> SET labels='706F6F.74657374' WHERE labels='666F6F.74657374';
```


These functions ensure that only correct names are stored:

```sql
sqlite> -- empty starting label
sqlite> INSERT INTO name (labels) VALUES ('.7476.74657374');
Error: Empty label in name
```

```sql
sqlite> -- empty ending label
sqlite> INSERT INTO name (labels) VALUES ('7476.74657374.');
Error: Empty label in name
```

```sql
sqlite> -- empty middle label
sqlite> INSERT INTO name (labels) VALUES ('7476..74657374');
Error: Empty label in name
```

```sql
sqlite> -- label with wrong number of digits
sqlite> INSERT INTO name (labels) VALUES ('7476.7.74657374');
Error: Improperly hex-encoded name
```

```sql
sqlite> -- uppercase characters
sqlite> UPDATE name
   ...> SET labels='4C6F6F.74657374' WHERE labels='706F6F.74657374';
Error: Name not lowercase
```

```sql
sqlite> -- non-hexidecimal characters
sqlite> INSERT INTO name (labels) VALUES ('7476.7X.74657374');
Error: Non-hexidecimal character in encoded name
sqlite> INSERT INTO name (labels) VALUES ('7476.X4.74657374');
Error: Non-hexidecimal character in encoded name
```

```sql
sqlite> -- label too long
sqlite> INSERT INTO name (labels) VALUES (printf('%0128d', '0'));
Error: Label too long
```

# Encoding & Decoding Presentation-Format - SQLite

SQLite has no stored procedures, so there is no direct way to
implement the encoding and decoding functionality that we have for
PostgreSQL and MySQL. There are approaches that can come close to this
functionality, but we'll leave the implementation of such encoding to
the applications for SQLite.
