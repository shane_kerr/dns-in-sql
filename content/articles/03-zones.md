---
title: "Zones"
date: 2022-10-20
draft: false
---

# Zone Name

DNS zones are identified by a name. On the Internet, each name is
unique, so for example there is only one `wikipedia.org`.

Sometimes a DNS zone is not available on the Internet, instead only
for local purposes. Or sometimes servers may return different versions
of a DNS zone on the Internet depending on metadata about the DNS
query message (for example, source address). If a database is used in
this way, zone names may _not_ be unique.

# Resource Record Sets (RRset)

A DNS zone contains of a number of DNS records, called resource
records (RR). These RR are collected into RRsets.

Each RRset is for a specific DNS name, and for a given DNS type
(`AAAA`, `NS`, and so on). There are a lot of DNS types, which are
listed on the [IANA DNS parameters
page](https://www.iana.org/assignments/dns-parameters/dns-parameters.xhtml#dns-parameters-4).

In principle a RRset is also associated with a given DNS class -
either Internet (`IN`), Chaos (`CH`), or Hessiod (`HS`) - but in
practice only `IN` is used for storage. (We'll omit class from our
tables, but they can be modified to add it if this becomes important.)

An RRset has:

- A DNS name, like `www.example.com`.
- A DNS class, almost always `IN` (Internet).
- A DNS type, like `AAAA` (IPv6 address) or `TXT` (text).
- A TTL (time-to-live), which is a number of seconds that the record
  can be safely cached.
- At least one type-specific data, called RDATA.

Each RR in an an RRset contains these items, all of which will be
identical except for the RDATA. As implied by the word "set", the
individual RR of an RRset are not ordered.

We will wait until the [next section](/dns-in-sql/articles/04-rdata/)
before considering RDATA further.

# RRSet within a Zone

At a minimum, a zone has the following RRset at the zone apex (meaning
at the "top" of the zone, with the same name as the zone):

* SOA (always with exactly 1 record)
* NS (recommended at least 2 RR, but any amount is possible)

# Implementations

Each implementation varies wildly depending on the features of the
database, and each is described on a separate article.

- [Zones - PostgreSQL](/dns-in-sql/articles/03-zones-postgresql/)
- [Zones - MySQL](/dns-in-sql/articles/03-zones-mysql/)
- [Zones - MariaDB](/dns-in-sql/articles/03-zones-mariadb/)
- [Zones - SQLite](/dns-in-sql/articles/03-zones-sqlite/)

---
[Record Data](/dns-in-sql/articles/04-rdata/)

