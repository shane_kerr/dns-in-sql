---
title: "Zones - SQLite"
date: 2022-10-09
draft: false
---

# DNS Types - SQLite

DNS types have a name and a number assigned by IANA, which we can
store in a simple table:

```sql
CREATE TABLE dns_type (
    dns_type_id INTEGER PRIMARY KEY CHECK (dns_type_id BETWEEN 0 AND 65535),
    code TEXT UNIQUE NOT NULL
);
```

In SQLite integers are self-sizing up to 8 bytes long, so we use a
`CHECK` constraint to limit the values to the allowed range.

And we can populate it with some types that we need to start with:

```sql
INSERT INTO dns_type (code, dns_type_id) VALUES
  ('NS', 2),
  ('SOA', 6),
  ('AAAA', 28);
```

# Zone Definition - SQLite

A simple zone definition is just the name (using the regular
expression check as described in the [invariant
checking](http://localhost:1313/dns-in-sql/articles/02-names-sqlite/#invariant-checking---sqlite).

```sql
CREATE TABLE zone (
    zone_id INTEGER PRIMARY KEY,
    origin TEXT NOT NULL UNIQUE
        CHECK (
          (length(origin) <= 503)
          AND ((origin = '')
            OR (origin REGEXP '^([0-36-9A-F][0-9A-F]|40|5[B-F]){1,63}(\.([0-36-9A-F][0-9A-F]|40|5[B-F]){1,63})*$')))
);
```

Before we create the `rrset` table, we should enable SQLite support
for foreign key constraints:

```sql
PRAGMA foreign_keys = ON;
```

> &#x26a0; **Warning**
>
> We need to set this on every connection to the database, since
> foreign key constraints are not enforced by default in SQLite and this
> pragma is not stored in the database itself.

The RRset table, also uses the name syntax for the name of the RRset:

```sql
CREATE TABLE rrset (
    rrset_id INTEGER PRIMARY KEY,
    zone_id INTEGER NOT NULL,
    name TEXT NOT NULL
        CHECK (
          (length(name) <= 503)
          AND ((name = '')
            OR (name REGEXP '^([0-36-9A-F][0-9A-F]|40|5[B-F]){1,63}(\.([0-36-9A-F][0-9A-F]|40|5[B-F]){1,63})*$'))),
    dns_type_id INTEGER NOT NULL,
    ttl INTEGER NOT NULL CHECK (ttl BETWEEN 0 AND 2147483647),
    FOREIGN KEY (zone_id) REFERENCES zone(zone_id) ON DELETE CASCADE,
    FOREIGN KEY (dns_type_id) REFERENCES dns_type(dns_type_id)
);

CREATE UNIQUE INDEX rrset_idx1 ON rrset (zone_id, name, dns_type_id);
```


It is a bit tricker populating a zone in SQLite directly from the
command line, and not really the intended way to work with the
database (it is expected that a program handles that). But we can
still do it:

```sql
BEGIN;
INSERT INTO zone (origin) VALUES ('6578616D706C65.666F6F');
INSERT INTO rrset (zone_id, name, dns_type_id, ttl) VALUES
(
  (SELECT zone_id FROM zone WHERE origin='6578616D706C65.666F6F'),
  '',
  (SELECT dns_type_id FROM dns_type WHERE code='SOA'),
  60*5
),
(
  (SELECT zone_id FROM zone WHERE origin='6578616D706C65.666F6F'),
  '',
  (SELECT dns_type_id FROM dns_type WHERE code='NS'),
  60*5
);
COMMIT;
```

We now have a populated zone:

```sql
sqlite> SELECT origin || iif(name = '', '', '.' || name), ttl, dns_type.code
        FROM rrset, zone, dns_type
        WHERE zone.zone_id = rrset.zone_id
        AND dns_type.dns_type_id = rrset.dns_type_id
        ORDER BY name, rrset_id;
6578616D706C65.666F6F|300|SOA
6578616D706C65.666F6F|300|NS
```

It's a bit tricky to read since we don't have a handy way to decode
the names, but the data is there.

# Invariant Checking - SQLite

We have several additional constraints on our data that we should add.

- There must be exactly one `SOA` rrset for the zone.
- There must be an `NS` rrset with the same name as the zone.

We could create a trigger which enforced validate these constraints.
Unfortunately this would need to be executed at the end of a
transaction that created at least 3 rows (the zone, an rrset for the
`SOA`, and an rrset for the `NS`). This requires a deferred check,
which is only supported in SQLite for foreign key constraints
(although the ISO SQL:1999 standard _does_ support deferred checks, so
in principle this is possible).

To work around this limitation, we need to denormalize our data a bit.
Rather than storing our `SOA` and `NS` in a separate table, we will
store them as part of our zone table:

```sql
CREATE TABLE zone (
    zone_id INTEGER PRIMARY KEY,
    origin TEXT NOT NULL UNIQUE
        CHECK (
          (length(origin) <= 503)
          AND ((origin = '')
            OR (origin REGEXP '^([0-36-9A-F][0-9A-F]|40|5[B-F]){1,63}(\.([0-36-9A-F][0-9A-F]|40|5[B-F]){1,63})*$'))),

    -- SOA fields
    soa_ttl INTEGER NOT NULL CHECK (soa_ttl BETWEEN 0 AND 2147483647),
    soa_mname TEXT NOT NULL
        CHECK (
          (length(soa_mname) <= 503)
          AND ((soa_mname = '')
            OR (soa_mname REGEXP '^([0-36-9A-F][0-9A-F]|40|5[B-F]){1,63}(\.([0-36-9A-F][0-9A-F]|40|5[B-F]){1,63})*$'))),
    soa_rname TEXT NOT NULL
        CHECK (
          (length(soa_rname) <= 503)
          AND ((soa_rname = '')
            OR (soa_rname REGEXP '^([0-36-9A-F][0-9A-F]|40|5[B-F]){1,63}(\.([0-36-9A-F][0-9A-F]|40|5[B-F]){1,63})*$'))),
    soa_serial INTEGER NOT NULL CHECK (soa_serial BETWEEN 0 AND 4294967296),
    soa_refresh INTEGER NOT NULL CHECK (soa_refresh BETWEEN 0 AND 4294967296),
    soa_retry INTEGER NOT NULL CHECK (soa_retry BETWEEN 0 AND 4294967296),
    soa_expire INTEGER NOT NULL CHECK (soa_expire BETWEEN 0 AND 4294967296),
    soa_minimum INTEGER NOT NULL CHECK (soa_minimum BETWEEN 0 AND 4294967296),

    -- first NS
    ns_ttl INTEGER NOT NULL CHECK (ns_ttl BETWEEN 0 AND 2147483647),
    ns_nsdname TEXT NOT NULL
        CHECK (
          (length(ns_nsdname) <= 503)
          AND ((ns_nsdname = '')
            OR (ns_nsdname REGEXP '^([0-36-9A-F][0-9A-F]|40|5[B-F]){1,63}(\.([0-36-9A-F][0-9A-F]|40|5[B-F]){1,63})*$')))
);
```

We do not need separate name or type information for the `SOA` or `NS`
values here, as they live at the zone apex and have specific types. We
do need the TTL as that can be different.

Storing the `SOA` and `NS` values with the zone does introduce some
complexity. It means that it is not possible to find all of the
RRset for zone by looking in a single table. Also, for the `NS` values
it means that while the first `NS` value is stored in the zone table,
other `NS` values are stored in a different table. This will slightly
complicate our `NS` data, as we see later.

Now creating a zone is a single `INSERT`:

```sql
sqlite> INSERT INTO zone (
    origin,
    soa_ttl,
    soa_mname, soa_rname, soa_serial,
    soa_refresh, soa_retry, soa_expire, soa_minimum,
    ns_ttl,
    ns_nsdname
) VALUES (
    '6578616D706C65.626172',
    -- SOA
    600,
    '6578616D706C65.626172.6E7331',
    '6578616D706C65.626172.61646D696E',
    strftime('%s'),
    900,
    300,
    86400*10,
    600,
    -- NS
    600,
    '6578616D706C65.626172.6E7331'
);
```

We can also add a check preventing `SOA` types for rows in the rrset
table, since these are now stored in the zone record. Likewise, we
will only allow `NS` rrset that are not at the apex:

```sql
CREATE TABLE rrset (
    rrset_id INTEGER PRIMARY KEY,
    zone_id INTEGER NOT NULL,
    name TEXT NOT NULL
        CHECK (
          (length(name) <= 503)
          AND ((name = '')
            OR (name REGEXP '^([0-36-9A-F][0-9A-F]|40|5[B-F]){1,63}(\.([0-36-9A-F][0-9A-F]|40|5[B-F]){1,63})*$'))),
    dns_type_id INTEGER NOT NULL,
    ttl INTEGER NOT NULL CHECK (ttl BETWEEN 0 AND 2147483647),
    FOREIGN KEY (zone_id) REFERENCES zone(zone_id) ON DELETE CASCADE,
    FOREIGN KEY (dns_type_id) REFERENCES dns_type(dns_type_id),
    CHECK ((dns_type_id <> 6) AND ((dns_type_id <> 2) OR (name <> '')))
);

CREATE UNIQUE INDEX rrset_idx1 ON rrset (zone_id, name, dns_type_id);
```

We can still create other RRset:

```sql
sqlite> INSERT INTO rrset (zone_id, name, dns_type_id, ttl)
        VALUES
        (
          (SELECT zone_id FROM zone WHERE origin='6578616D706C65.626172'),
          '737562646F6D61696E',
          (SELECT dns_type_id FROM dns_type WHERE code='NS'),
          60*5
        ),
        (
          (SELECT zone_id FROM zone WHERE origin='6578616D706C65.626172'),
          '',
          (SELECT dns_type_id FROM dns_type WHERE code='AAAA'),
          60*5
        );
```

However we cannot make any other SOA in the zone:

```sql
sqlite> INSERT INTO rrset (zone_id, name, dns_type_id, ttl)
        VALUES
        (
          (SELECT zone_id FROM zone WHERE origin='6578616D706C65.626172'),
          '666F6F',
          (SELECT dns_type_id FROM dns_type WHERE code='SOA'),
          60*5
        );
Error: CHECK constraint failed: (dns_type_id <> 6) AND ((dns_type_id <> 2) OR (name <> ''))
```

And we cannot add an NS at the zone apex:

```sql
sqlite> INSERT INTO rrset (zone_id, name, dns_type_id, ttl)
        VALUES
        (
          (SELECT zone_id FROM zone WHERE origin='6578616D706C65.626172'),
          '',
          (SELECT dns_type_id FROM dns_type WHERE code='NS'),
          60*5
        );
Error: CHECK constraint failed: (dns_type_id <> 6) AND ((dns_type_id <> 2) OR (name <> ''))
```

Since different RR type have different syntax and semantics, we don’t
want an RR to be able to change type after it is created. We can
prevent this with a trigger:

```sql
CREATE TRIGGER rrset_prevent_type_change
BEFORE UPDATE
ON rrset
WHEN NEW.dns_type_id <> OLD.dns_type_id
BEGIN
  SELECT RAISE(ABORT, 'RRset cannot change type');
END;
```

This works:

```sql
sqlite> UPDATE rrset
        SET dns_type_id=128
        WHERE
          zone_id=(SELECT zone_id FROM zone WHERE origin='6578616D706C65.626172')
          AND name='737562646F6D61696E';
Error: RRset cannot change type
```

While we unfortunately had to denormalize our data, our SQLite
implementation gives us comprehensive checks on the data, and should
only allow creation of only correct zones.
