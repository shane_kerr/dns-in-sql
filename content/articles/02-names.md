---
title: "Names"
date: 2022-09-29
draft: false
---

# Storing a Name

The simplest thing that we'll want to do is to store a DNS name. A
name in DNS consists of a number of labels, separated by dots, like
this:

```
www.jobs.ox.ac.uk
```

A label can have any bytes, but has to be between 1 and 63 bytes long.
Non-printable characters - and ones with special meaning like the
`.` (dot) - can be escaped:

```
no-dot.with-\.-escaped.dot
no-dot.with-\046-escaped.dot
```

The total length of a name cannot be more than 255 bytes in wire
format (the wire format is how the name is encoded in the messages
used by DNS software to talk over a network).

# Operations on Names

There are a few operations that we typically need to do on names:

1. Find an exact match for a name.
2. Find the parent of a name.
3. Find the previous name.

The first two are necessary for answering any DNS query, and the last
one is useful for answering DNSSEC queries.

In DNS, sorting order is by labels from the right most side, so we
the following list of names would be sorted in this order:

- `admin\.email.example`
- `abc.order.example`
- `\123.order.example`
- `bar.test`
- `foo.test`
- `\000.tv.test`

To achieve this, we can reverse the order of labels in each name:

- `example.admin\.email`
- `example.order.abc`
- `example.order.\123`
- `test.bar`
- `test.foo`
- `test.tv.\000`

Now the desired sort order matches a normal ASCII sort order.

The ideal format for storing a name is probably something like an
array of binary labels, stored in reverse order:

```
www.jobs.ox.ac.uk -> [ "uk", "ac", "ox", "jobs", "www" ]
```

DNS allows any data in labels, and binary labels can store these
values.

The reversed array allows efficient exact-match and parent/child
searches, as well as efficient searches for the previous name in
canonical DNS sorted order (generally nice, but also necessary for
some DNSSEC and digest creation operations).

With this approach finding an exact match is simple, by finding the
entry with the same labels that is being searched.

Finding the parent of a name is also simple, which we can do by
looking for the longest matching set of labels.

Finding the previous record to a name just involves a less-than
comparison for each label instead of an equality comparison.

# Implementations

Each implementation varies wildly depending on the features of the
database, and each is described on a separate article.

- [Names - PostgreSQL](/dns-in-sql/articles/02-names-postgresql/)
- [Names - MySQL](/dns-in-sql/articles/02-names-mysql/)
- [Names - MariaDB](/dns-in-sql/articles/02-names-mariadb/)
- [Names - SQLite](/dns-in-sql/articles/02-names-sqlite/)
 
---
[Zones](/dns-in-sql/articles/03-zones/)

