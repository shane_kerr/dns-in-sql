---
title: "Names - MariaDB"
date: 2022-09-29
draft: false
---

# Name Storage - MariaDB

MariaDB is intended to be compatible with MySQL, and it mostly is. The
two databases have diverged slightly so we'll keep a separate page for
MariaDB, although it largely just references the MySQL page.

We use the exact same format, for the same reason, as in
[MySQL](http://localhost:1313/dns-in-sql/articles/02-names-mysql/#invariant-checking---mysql):

```mysql
CREATE TABLE name (
    name_id INT AUTO_INCREMENT PRIMARY KEY,
    labels VARCHAR(503) NOT NULL
        CHECK (
          labels REGEXP '^([0-36-9A-F][0-9A-F]|40|5[B-F]){1,63}(\\.([0-36-9A-F][0-9A-F]|40|5[B-F]){1,63})*$'
          OR labels = ''),
    UNIQUE INDEX (labels)
);
```

# Encoding a Presentation-Format Label - MariaDB

This works identically to
[MySQL](http://localhost:1313/dns-in-sql/articles/02-names-mysql/#encoding-a-presentation-format-label---mysql).

# Encoding a Presentation-Format Name - MariaDB

Unfortunately we have to modify this slightly from the MySQL version.
The [`REGEXP_INSTR()`](https://mariadb.com/kb/en/regexp_instr/)
function in MariaDB does not support the `pos` argument, which tells
us where to start the search.  We can work around this using 
[`SUBSTR()`](https://mariadb.com/kb/en/substring/).

```mysql
DELIMITER $$
CREATE FUNCTION dns_name_encode (name VARCHAR(1003))
RETURNS VARCHAR(503) DETERMINISTIC NO SQL
BEGIN
  DECLARE encoded_name VARCHAR(503);
  DECLARE p INT;
  DECLARE dot INT;

  -- Handle the root zone.
  IF name = '.' THEN
    RETURN '';
  END IF;

  SET encoded_name = '';
  SET p = 1;
  scan: LOOP

    -- If there is a '.' at the beginning of a label, then we have
    -- an empty label. This is an error.
    IF SUBSTR(name, p, 1) = '.' THEN
      SIGNAL SQLSTATE '45000'
        SET MESSAGE_TEXT = 'Empty label in name';
    END IF;

    -- Find the next non-escaped '.' in our name.
    SET dot = REGEXP_INSTR(SUBSTR(name, p), '[^\\\\]\\.');

    -- If no more '.', prepend the final encoded label and we're done
    IF dot = 0 THEN
      SET encoded_name = CONCAT(dns_label_encode(SUBSTR(name, p)),
                                encoded_name);
      LEAVE scan;
    END IF;

    -- Otherwise, prepend the encoded label with a '.' before it.
    SET encoded_name = CONCAT('.',
                              dns_label_encode(SUBSTR(name, p, dot)),
                              encoded_name);
    SET p = p + dot + 1;

    -- Handle a trailing '.'.
    IF p > LENGTH(name) THEN
      SET encoded_name = SUBSTR(encoded_name, 2);
      LEAVE scan;
    END IF;

  END LOOP;
  RETURN encoded_name;
END $$

DELIMITER ;
```

Usage is identical.

# Decoding a Label to Presentation-Format - MariaDB

This works identically to
[MySQL](http://localhost:1313/dns-in-sql/articles/02-names-mysql/#decoding-a-label-to-presentation-format---mysql).

# Decoding a Name to Presentation-Format - MariaDB

This works identically to
[MySQL](http://localhost:1313/dns-in-sql/articles/02-names-mysql/#decoding-a-name-to-presentation-format---mysql).
