---
title: "Zones - PostgreSQL"
date: 2022-10-05
draft: false
---

# DNS Types - PostgreSQL

DNS types have a name and a number assigned by IANA, which we can
store in a simple table:

```postgres
CREATE TABLE dns_type (
    dns_type_id INT PRIMARY KEY CHECK (dns_type_id BETWEEN 0 AND 65535),
    code VARCHAR(10) UNIQUE NOT NULL
);

CREATE INDEX ON dns_type (dns_type_id, code);
CREATE INDEX ON dns_type (code, dns_type_id);
```

There is no built-in unsigned integer type in PostgreSQL, so we use a
bigger size than we need (`INT` instead of `SMALLINT`) and add a
`CHECK` constraint to limit the values to the actual possible range.

We also create a pair of compound indexes. This allows for index-only
queries when we match either by number of symbol.

We can populate it with some types that we need to start with:

```postgres
INSERT INTO dns_type (code, dns_type_id) VALUES
  ('NS', 2),
  ('SOA', 6),
  ('AAAA', 28);
```

# Zone Definition - PostgreSQL

Our zone definition is just the name (using `is_valid_name()` as
described in the [invariant
checking](/dns-in-sql/articles/02-names-postgresql/#invariant-checking---postgresql)):

```postgres
CREATE TABLE zone (
    zone_id INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    origin BYTEA[] UNIQUE NOT NULL CHECK (is_valid_name(origin))
);
```

For the RRset definition, we include include the zone that the RRset
is in, the type, and the TTL. For the RRset name, we use a relative
name; that is, the name removing the zone name from the end.

For example if we were storing an RRset for `www.foo.test` that was in
the zone `foo.test`, we would only store the name `www`.

```postgres
CREATE TABLE rrset (
    rrset_id INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    zone_id INT NOT NULL,
    name BYTEA[] NOT NULL CHECK (is_valid_name(name)),
    dns_type_id INT NOT NULL,
    ttl INT NOT NULL CHECK (ttl BETWEEN 0 AND 2147483647),
    FOREIGN KEY (zone_id) REFERENCES zone(zone_id) ON DELETE CASCADE,
    FOREIGN KEY (dns_type_id) REFERENCES dns_type(dns_type_id)
);

CREATE UNIQUE INDEX ON rrset (zone_id, name, dns_type_id);
```

We have the following checks here:

- Foreign key to enforce that the zone exists.
- Name validity checking for the relative part of the name.
- Foreign key to enforce that the type exists.
- TTL range checking (based on
  [RFC 2181](https://www.rfc-editor.org/rfc/rfc2181.html#page-10)).

The `UNIQUE` index enforces the protocol-level uniqueness requirement.

We cascade deletes from the zone to RR in that zone. That way deleting
the entry in the `zone` table will automatically clean up all records
for that zone.

Using these tables we can create a minimal zone.

```postgres
INSERT INTO zone (origin) VALUES (dns_name_encode('foo.example'));
INSERT INTO rrset (zone_id, name, dns_type_id, ttl) VALUES
  (
    (SELECT zone_id FROM zone WHERE origin=dns_name_encode('foo.example')),
    dns_name_encode('.'),
    (SELECT dns_type_id FROM dns_type WHERE code='SOA'),
    60*5
  ),
  (
    (SELECT zone_id FROM zone WHERE origin=dns_name_encode('foo.example')),
    dns_name_encode('.'),
    (SELECT dns_type_id FROM dns_type WHERE code='NS'),
    60*5
  );
```

We now have a zone with our required two RRset:

```postgres
dns=> SELECT dns_name_decode(origin || name) AS name, ttl, dns_type.code
dns-> FROM zone, rrset, dns_type
dns-> WHERE (dns_type.dns_type_id = rrset.dns_type_id)
dns->     AND (zone.zone_id = rrset.zone_id)
dns-> ORDER BY name, rrset.dns_type_id;
    name     | ttl | type
-------------+-----+------
 foo.example | 300 | SOA
 foo.example | 300 | NS
(2 rows)
```

# Invariant Checking - PostgreSQL

We have the following constraints that we would like to enforce:

- There must be an `SOA` RRset with the same name as the zone.
- There must be an `NS` RRset with the same name as the zone.

In PostgreSQL we can defer certain operations until a transaction is
complete. Sadly we cannot do this for `CHECK` constraints, but we can
create a deferred constraint trigger.

Lets make a procedure that checks for both `SOA` and `NS` RRset at a
given name:

```postgres
CREATE OR REPLACE FUNCTION zone_required_rrset ()
RETURNS trigger
AS $$
DECLARE
  rrsets_exist BOOLEAN;
BEGIN
  SELECT EXISTS (
      SELECT rrset_id
      FROM rrset
      WHERE rrset.zone_id=NEW.zone_id AND rrset.dns_type_id=2)
    AND EXISTS (
      SELECT rrset_id
      FROM rrset
      WHERE rrset.zone_id=NEW.zone_id AND rrset.dns_type_id=6)
  INTO rrsets_exist;
  ASSERT rrsets_exist, 'Both SOA and NS required at zone apex';
  RETURN NEW;
END;
$$ LANGUAGE plpgsql;
```

Now we can use this in our zone table definition:

```postgres
CREATE CONSTRAINT TRIGGER zone_required_rrset AFTER INSERT
ON zone
INITIALLY DEFERRED
FOR EACH ROW
EXECUTE PROCEDURE zone_required_rrset();
```

Trying to insert a zone by itself now fails:

```postgres
dns=> INSERT INTO zone (origin) VALUES (dns_name_encode('bar.example'));
ERROR:  Both SOA and NS required at zone apex
CONTEXT:  PL/pgSQL function zone_required_rrset() line 14 at ASSERT
```

We can do this as separate statements within a transaction though:

```postgres
BEGIN;
INSERT INTO zone (origin) VALUES (dns_name_encode('bar.example'));
INSERT INTO rrset (zone_id, name, dns_type_id, ttl) VALUES
(
  (SELECT zone_id FROM zone WHERE origin=dns_name_encode('bar.example')),
  dns_name_encode('.'),
  (SELECT dns_type_id FROM dns_type WHERE code='SOA'),
  60*5
),
(
  (SELECT zone_id FROM zone WHERE origin=dns_name_encode('bar.example')),
  dns_name_encode('.'),
  (SELECT dns_type_id FROM dns_type WHERE code='NS'),
  60*5
);
COMMIT;
```

We can also use a CTE (common table expression) do this in a single
statement, without explicitly using a transaction:

```postgres
-- First create our new zone
WITH new_zone AS (
  INSERT INTO zone (origin) VALUES (dns_name_encode('baz.example'))
  RETURNING zone_id
),
-- Next define our SOA and NS RRset
build_rows AS (
  SELECT name, dns_type_id, ttl
  FROM (
    VALUES
    (
      dns_name_encode('.'),
      (SELECT dns_type_id FROM dns_type WHERE code='SOA'),
      60*5
    ),
    (
      dns_name_encode('.'),
      (SELECT dns_type_id FROM dns_type WHERE code='NS'),
      60*5
    )
  ) AS row_data(name, dns_type_id, ttl)
)
-- Finally combine the zone_id and RRset data into new records
INSERT INTO rrset (zone_id, name, dns_type_id, ttl)
SELECT zone_id, name, dns_type_id, ttl
FROM new_zone, build_rows;
```

We also need to create a restriction on RRset, so that the necessary
RRset for a zone are not deleted or modified after zone creation.

For the deletion:

```postgres
CREATE OR REPLACE FUNCTION rrset_required_delete_check ()
RETURNS trigger
AS $$
DECLARE
  zone_exists BOOLEAN;
BEGIN
  SELECT EXISTS (
      SELECT zone_id
      FROM zone
      WHERE zone.zone_id=OLD.zone_id)
  INTO zone_exists;
  IF zone_exists THEN
    IF OLD.dns_type_id = 6  THEN
      RAISE EXCEPTION 'Cannot delete SOA from zone';
    END IF;
    RAISE EXCEPTION 'Cannot delete NS from zone apex';
  END IF;
  RETURN NEW;
END;
$$ LANGUAGE plpgsql STABLE;
```

```postgres
CREATE CONSTRAINT TRIGGER rrset_required_delete_check AFTER DELETE
ON rrset
INITIALLY DEFERRED
FOR EACH ROW
WHEN (((OLD.dns_type_id = 2) OR (OLD.dns_type_id = 6)) AND (OLD.name = '{}'))
EXECUTE FUNCTION rrset_required_delete_check();
```

We have to use `INITIALLY DEFERRED` or the trigger also prevents zone
deletion, since the cascaded deletes will prevent removing this RRset.

Now attempting to delete the `SOA` or `NS` RRset at the apex fail:

```postgres
dns=> DELETE FROM rrset
dns-> WHERE name=dns_name_encode('.')
dns->   AND zone_id=(SELECT zone_id FROM zone
dns->                WHERE origin=dns_name_encode('foo.example'))
dns->   AND dns_type_id=(SELECT dns_type_id FROM dns_type WHERE code='SOA');
ERROR:  Cannot delete SOA from zone
CONTEXT:  PL/pgSQL function rrset_required_delete_check() line 8 at ASSERT
```

We do a similar check when a RRset is modified:

```postgres
CREATE OR REPLACE FUNCTION rrset_required_update_check ()
RETURNS trigger
AS $$
BEGIN
  RAISE EXCEPTION 'Update to RRset removes required record from zone';
  RETURN NEW;
END;
$$ LANGUAGE plpgsql STABLE;
```

```postgres
CREATE CONSTRAINT TRIGGER rrset_required_update_check AFTER UPDATE
ON rrset
FOR EACH ROW
WHEN (
    ((OLD.dns_type_id = 2) OR (OLD.dns_type_id = 6)) AND (OLD.name = '{}')
        AND (NEW.name <> '{}')
)
EXECUTE FUNCTION rrset_required_update_check();
```

Now we're protected from modification breaking our constraints too:

```postgres
dns=> UPDATE rrset SET name=dns_name_encode('xxx')
dns-> WHERE name=dns_name_encode('.')
dns->   AND zone_id=(SELECT zone_id FROM zone
dns->                WHERE origin=dns_name_encode('foo.example'))
dns->   AND dns_type_id=(SELECT dns_type_id FROM dns_type WHERE code='NS');
ERROR:  Update to RRset removes required record from zone
CONTEXT:  PL/pgSQL function rrset_required_update_check() line 3 at RAISE
```

We can also use a `CHECK` constraint to make sure that `SOA` are only
at the zone apex:

```postgres
CREATE TABLE rrset (
    rrset_id INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    zone_id INT NOT NULL,
    name BYTEA[] NOT NULL CHECK (is_valid_name(name)),
    dns_type_id INT NOT NULL,
    ttl INT NOT NULL CHECK (ttl BETWEEN 0 AND 2147483647),
    FOREIGN KEY (zone_id) REFERENCES zone(zone_id) ON DELETE CASCADE,
    FOREIGN KEY (dns_type_id) REFERENCES dns_type(dns_type_id),
    CHECK ((dns_type_id <> 6) OR (name = '{}'))
);

CREATE UNIQUE INDEX ON rrset (zone_id, name, dns_type_id);
```

Now attempting to insert an `SOA` RRset anywhere but the zone apex
fails:

```postgres
dns=> BEGIN;
BEGIN
dns=*> INSERT INTO rrset (zone_id, name, dns_type_id, ttl) VALUES
(
  (SELECT zone_id FROM zone WHERE origin=dns_name_encode('bar.example')),
  dns_name_encode('bad-soa'),
  (SELECT dns_type_id FROM dns_type WHERE code='SOA'),
  60*5
);
ERROR:  new row for relation "rrset" violates check constraint "rrset_check"
DETAIL:  Failing row contains (1, 5, {"\\x6261642d736f61"}, 6, 300).
```

Since different RR type have different syntax and semantics, we don't
want an RR to be able to change type after it is created. We can
prevent this with a trigger:

```postgres
CREATE OR REPLACE FUNCTION rrset_type_change_check ()
RETURNS trigger
AS $$
BEGIN
  RAISE EXCEPTION 'Update to RRset changes the type';
  RETURN NEW;
END;
$$ LANGUAGE plpgsql STABLE;
```

```postgres
CREATE CONSTRAINT TRIGGER rrset_type_change_check AFTER UPDATE
ON rrset
FOR EACH ROW
WHEN (OLD.dns_type_id <> NEW.dns_type_id)
EXECUTE FUNCTION rrset_type_change_check();
```

We can test this to make sure it works:

```postgres
dns=> INSERT INTO rrset (zone_id, name, dns_type_id, ttl) VALUES (
    (SELECT zone_id FROM zone WHERE origin=dns_name_encode('foo.example')),
    dns_name_encode('test'),
    (SELECT dns_type_id FROM dns_type where code='NS'),
    900);
INSERT 0 1
dns=> UPDATE rrset SET dns_type_id=28 WHERE
  zone_id=(select zone_id from zone where origin=dns_name_encode('foo.example'))
  AND name=dns_name_encode('test');
ERROR:  Update to RRset changes the type
CONTEXT:  PL/pgSQL function rrset_type_change_check() line 3 at RAISE
dns=>
```

Our PostgreSQL implementation gives us comprehensive checks on the
data, and should only allow creation of only correct zones.
