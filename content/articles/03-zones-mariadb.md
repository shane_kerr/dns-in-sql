---
title: "Zones - MariaDB"
date: 2022-10-09
draft: false
---

# DNS Types - MariaDB

This is identical to the [MySQL DNS
Types](https://shane_kerr.gitlab.io/dns-in-sql/articles/03-zones-mysql/#dns-types---mysql).

# Zone Definition - MariaDB

This is identical to the [MySQL Zone
Definition](https://shane_kerr.gitlab.io/dns-in-sql/articles/03-zones-mysql/#zone-definition---mysql).

# Invariant Checking - MariaDB

This is identical to the [MySQL Invariant
Checking](https://shane_kerr.gitlab.io/dns-in-sql/articles/03-zones-mysql/#invariant-checking---mysql).
